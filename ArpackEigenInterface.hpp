#ifndef ARPACKEIGENINTERFACE_HPP
#define ARPACKEIGENINTERFACE_HPP

#include <type_traits>
#include <iostream>
#include "ArpackEigenWorkspace.hpp"

/*
  Provides access to the unsymmetric Arpack routines. Complex ones definetively work
  Real ones should be tested.

  Mode 1: Normal Eigenvalue problem
  Mode 2: Generalized Eigenvalue problem
  Mode 3: Shift invert eigenvalues
 */
template<typename _MatrixType, int Mode>
class UnsymArpack {
 public:
  using MatrixType = _MatrixType;

  enum {
    RowsAtCompileTime = MatrixType::RowsAtCompileTime,
    ColsAtCompileTime = MatrixType::ColsAtCompileTime,
    Options = MatrixType::Options,
    MaxRowsAtCompileTime = MatrixType::MaxRowsAtCompileTime,
    MaxColsAtCompileTime = MatrixType::MaxColsAtCompileTime
  };

  using Scalar = typename MatrixType::Scalar;
  using Index = typename MatrixType::Index;
  using RealScalar = typename Eigen::NumTraits<Scalar>::Real;
  using ComplexScalar = std::complex<RealScalar>;
  using EigenvalueType =  Eigen::Matrix<ComplexScalar,
                                        ColsAtCompileTime,
                                        1, Options&(~Eigen::RowMajor),
                                        MaxColsAtCompileTime, 1>;

  using EigenvectorType =  Eigen::Matrix<ComplexScalar,
                                         RowsAtCompileTime,
                                         ColsAtCompileTime,
                                         Options,
                                         MaxRowsAtCompileTime,
                                         MaxColsAtCompileTime>;
  using Workspace_t = UnsymWorkspace<Scalar>;

  UnsymArpack()
      :m_eivec(),
       m_eival(),
       m_isInitialized(false),
       m_eigenvectorsOk(false),
       m_nbrConverged(0),
       m_nbrIterations(0) {}

  /*
    Constructor, that immediately sets all options.
    A : lhs matrix
    int nev: number of eigenvalues / vectors to be computed
    maxite: maximum number of arnoldi iterations, 0 Arpack default
    howmny: should be ritz vectors, when eigenvectors should be calculated
   */
  UnsymArpack(const MatrixType & A,
              int nev,
              bool rvec = true,
              int maxiter = 0,
              RealScalar tol = 0.0,
              howmny howmny_option = howmny::ritz_vectors,
              bmat bmat_option = bmat::identity,
              which ritz_option = which::largest_magnitude,
              const MatrixType & B = MatrixType(),
              int mode = 1,
              ComplexScalar sigma = 0.0,
              bool provide_shifts = false,
              const DynamicDvec<Scalar> & resid = dense_vec(),
              const DynamicDvec<int> & select = DynamicDvec<int>())
      :m_eivec(),
       m_eival(),
       m_isInitialized(false),
       m_eigenvectorsOk(false),
       m_nbrConverged(0),
       m_nbrIterations(0) {
    Workspace_t workspace(boost::numeric_cast<int>(A.rows()), nev, rvec,
                          howmny_option, bmat_option,
                          ritz_option, tol, mode, maxiter, sigma,
                          provide_shifts, resid, select);

    if (Mode == 1) {
      compute(A, B, workspace);
    }
    else if (Mode == 2) {
      using Solver = Eigen::SparseLU<MatrixType>;
      Solver sparse_solver(B);
      compute(sparse_solver.solve(A), B, workspace);
    }
    else if(Mode == 3) {
      using Solver = Eigen::SparseLU<MatrixType>;
      Solver sparse_solver(A - sigma * B);
      compute(sparse_solver.solve(B), B, workspace);
    }
  }

  using dense_vec = DynamicDvec<Scalar>;
  using Map_t = Eigen::Map<dense_vec>;

  // TODO: introduce sigma
  // template <typename = typename std::enable_if<Mode == 1 >::type >
  inline void Op(const MatrixType & A, Map_t X, Map_t Y) {
    Y = A * X;
  }

  bool execute_communication(int ido, Workspace_t & workspace,
                             const MatrixType & A, const MatrixType & B) {
    switch (ido) {
      case -1: Op(A, workspace.X(), workspace.Y());
        break;
      case 1: Op(A, workspace.X(), workspace.Y());
        break;
      case 2: Op(B, workspace.X(), workspace.Y());
        break;
      case 3: assert(false && "Not yet implemented");
        break;
      case 99: return false;
        break;
    }
    return true;
  }

  void compute(const MatrixType & A, const MatrixType & B,
               Workspace_t & workspace) {
    bool iteration = true;
    int ido = 0;
    while (iteration) {
      ido = workspace.call_naupd(ido);
      iteration = execute_communication(ido, workspace, A, B);
    }

    if (workspace.info() != 0) {
      std::cout << workspace.info() << std::endl;
      throw std::runtime_error("workspace info not 0");
    }

    workspace.call_neupd();
    if (workspace.info() != 0) {
      std::cout << workspace.info() << std::endl;
      throw std::runtime_error("workspace info not 0 after neupd");
    }

    // Now move data to this class from the workspace
    if (workspace.calculate_vectors()) {
      m_eivec = std::move(workspace.ritz_vectors());
      m_eivec.conservativeResize(A.rows(), workspace.nconv());
      m_eivec.colwise().normalize();
    }

    m_eival = std::move(workspace.ritz_approximants());
    m_eival.conservativeResize(workspace.nconv());
  }

  const EigenvectorType & eigenvectors() {
    return m_eivec;
  }

  const EigenvalueType & eigenvalues() {
    return m_eival;
  }

 protected:
  EigenvectorType m_eivec;
  EigenvalueType m_eival;
  bool m_isInitialized;
  bool m_eigenvectorsOk;

  size_t m_nbrConverged;
  size_t m_nbrIterations;
};

#endif //  ARPACKEIGENINTERFACE_HPP 
