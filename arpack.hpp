#ifndef ARPACK_HPP
#define ARPACK_HPP

#include <complex>
// #include "complex.h"

namespace detail {

extern "C" {

  /*
    c\Name: cnaupd
    c
    c\Description: 
    c  Reverse communication interface for the Implicitly Restarted Arnoldi
    c  iteration. This is intended to be used to find a few eigenpairs of a 
    c  complex linear operator OP with respect to a semi-inner product defined 
    c  by a hermitian positive semi-definite real matrix B. B may be the identity 
    c  matrix.  NOTE: if both OP and B are real, then ssaupd or snaupd should
    c  be used.
    c
    c
    c  The computed approximate eigenvalues are called Ritz values and
    c  the corresponding approximate eigenvectors are called Ritz vectors.
    c
    c  cnaupd is usually called iteratively to solve one of the 
    c  following problems:
    c
    c  Mode 1:  A*x = lambda*x.
    c           ===> OP = A  and  B = I.
    c
    c  Mode 2:  A*x = lambda*M*x, M hermitian positive definite
    c           ===> OP = inv[M]*A  and  B = M.
    c           ===> (If M can be factored see remark 3 below)
    c
    c  Mode 3:  A*x = lambda*M*x, M hermitian semi-definite
    c           ===> OP =  inv[A - sigma*M]*M   and  B = M. 
    c           ===> shift-and-invert mode 
    c           If OP*x = amu*x, then lambda = sigma + 1/amu.
    c  
    c
    c  NOTE: The action of w <- inv[A - sigma*M]*v or w <- inv[M]*v
    c        should be accomplished either by a direct method
    c        using a sparse matrix factorization and solving
    c
    c           [A - sigma*M]*w = v  or M*w = v,
    c
    c        or through an iterative method for solving these
    c        systems.  If an iterative method is used, the
    c        convergence test must be more stringent than
    c        the accuracy requirements for the eigenvalue
    c        approximations.
    c
    c\Usage:
    c  call cnaupd
    c     ( IDO, BMAT, N, WHICH, NEV, TOL, RESID, NCV, V, LDV, IPARAM,
    c       IPNTR, WORKD, WORKL, LWORKL, RWORK, INFO )
    c
    c\Arguments
    c  IDO     Integer.  (INPUT/OUTPUT)
    c          Reverse communication flag.  IDO must be zero on the first 
    c          call to cnaupd.  IDO will be set internally to
    c          indicate the type of operation to be performed.  Control is
    c          then given back to the calling routine which has the
    c          responsibility to carry out the requested operation and call
    c          cnaupd with the result.  The operand is given in
    c          WORKD(IPNTR(1)), the result must be put in WORKD(IPNTR(2)).
    c          -------------------------------------------------------------
    c          IDO =  0: first call to the reverse communication interface
    c          IDO = -1: compute  Y = OP * X  where
    c                    IPNTR(1) is the pointer into WORKD for X,
    c                    IPNTR(2) is the pointer into WORKD for Y.
    c                    This is for the initialization phase to force the
    c                    starting vector into the range of OP.
    c          IDO =  1: compute  Y = OP * X  where
    c                    IPNTR(1) is the pointer into WORKD for X,
    c                    IPNTR(2) is the pointer into WORKD for Y.
    c                    In mode 3, the vector B * X is already
    c                    available in WORKD(ipntr(3)).  It does not
    c                    need to be recomputed in forming OP * X.
    c          IDO =  2: compute  Y = M * X  where
    c                    IPNTR(1) is the pointer into WORKD for X,
    c                    IPNTR(2) is the pointer into WORKD for Y.
    c          IDO =  3: compute and return the shifts in the first 
    c                    NP locations of WORKL.
    c          IDO = 99: done
    c          -------------------------------------------------------------
    c          After the initialization phase, when the routine is used in 
    c          the "shift-and-invert" mode, the vector M * X is already 
    c          available and does not need to be recomputed in forming OP*X.
    c             
    c  BMAT    Character*1.  (INPUT)
    c          BMAT specifies the type of the matrix B that defines the
    c          semi-inner product for the operator OP.
    c          BMAT = 'I' -> standard eigenvalue problem A*x = lambda*x
    c          BMAT = 'G' -> generalized eigenvalue problem A*x = lambda*M*x
    c
    c  N       Integer.  (INPUT)
    c          Dimension of the eigenproblem.
    c
    c  WHICH   Character*2.  (INPUT)
    c          'LM' -> want the NEV eigenvalues of largest magnitude.
    c          'SM' -> want the NEV eigenvalues of smallest magnitude.
    c          'LR' -> want the NEV eigenvalues of largest real part.
    c          'SR' -> want the NEV eigenvalues of smallest real part.
    c          'LI' -> want the NEV eigenvalues of largest imaginary part.
    c          'SI' -> want the NEV eigenvalues of smallest imaginary part.
    c
    c  NEV     Integer.  (INPUT)
    c          Number of eigenvalues of OP to be computed. 0 < NEV < N-1.
    c
    c  TOL     Real   scalar.  (INPUT)
    c          Stopping criteria: the relative accuracy of the Ritz value 
    c          is considered acceptable if BOUNDS(I) .LE. TOL*ABS(RITZ(I))
    c          where ABS(RITZ(I)) is the magnitude when RITZ(I) is complex.
    c          DEFAULT = slamch('EPS')  (machine precision as computed
    c                    by the LAPACK auxiliary subroutine slamch).
    c
    c  RESID   Complex  array of length N.  (INPUT/OUTPUT)
    c          On INPUT: 
    c          If INFO .EQ. 0, a random initial residual vector is used.
    c          If INFO .NE. 0, RESID contains the initial residual vector,
    c                          possibly from a previous run.
    c          On OUTPUT:
    c          RESID contains the final residual vector.
    c
    c  NCV     Integer.  (INPUT)
    c          Number of columns of the matrix V. NCV must satisfy the two
    c          inequalities 1 <= NCV-NEV and NCV <= N.
    c          This will indicate how many Arnoldi vectors are generated 
    c          at each iteration.  After the startup phase in which NEV 
    c          Arnoldi vectors are generated, the algorithm generates 
    c          approximately NCV-NEV Arnoldi vectors at each subsequent update 
    c          iteration. Most of the cost in generating each Arnoldi vector is 
    c          in the matrix-vector operation OP*x. (See remark 4 below.)
    c
    c  V       Complex  array N by NCV.  (OUTPUT)
    c          Contains the final set of Arnoldi basis vectors. 
    c
    c  LDV     Integer.  (INPUT)
    c          Leading dimension of V exactly as declared in the calling program.
    c
    c  IPARAM  Integer array of length 11.  (INPUT/OUTPUT)
    c          IPARAM(1) = ISHIFT: method for selecting the implicit shifts.
    c          The shifts selected at each iteration are used to filter out
    c          the components of the unwanted eigenvector.
    c          -------------------------------------------------------------
    c          ISHIFT = 0: the shifts are to be provided by the user via
    c                      reverse communication.  The NCV eigenvalues of 
    c                      the Hessenberg matrix H are returned in the part
    c                      of WORKL array corresponding to RITZ.
    c          ISHIFT = 1: exact shifts with respect to the current
    c                      Hessenberg matrix H.  This is equivalent to 
    c                      restarting the iteration from the beginning 
    c                      after updating the starting vector with a linear
    c                      combination of Ritz vectors associated with the 
    c                      "wanted" eigenvalues.
    c          ISHIFT = 2: other choice of internal shift to be defined.
    c          -------------------------------------------------------------
    c
    c          IPARAM(2) = No longer referenced 
    c
    c          IPARAM(3) = MXITER
    c          On INPUT:  maximum number of Arnoldi update iterations allowed. 
    c          On OUTPUT: actual number of Arnoldi update iterations taken. 
    c
    c          IPARAM(4) = NB: blocksize to be used in the recurrence.
    c          The code currently works only for NB = 1.
    c
    c          IPARAM(5) = NCONV: number of "converged" Ritz values.
    c          This represents the number of Ritz values that satisfy
    c          the convergence criterion.
    c
    c          IPARAM(6) = IUPD
    c          No longer referenced. Implicit restarting is ALWAYS used.  
    c
    c          IPARAM(7) = MODE
    c          On INPUT determines what type of eigenproblem is being solved.
    c          Must be 1,2,3; See under \Description of cnaupd for the 
    c          four modes available.
    c
    c          IPARAM(8) = NP
    c          When ido = 3 and the user provides shifts through reverse
    c          communication (IPARAM(1)=0), _naupd returns NP, the number
    c          of shifts the user is to provide. 0 < NP < NCV-NEV.
    c
    c          IPARAM(9) = NUMOP, IPARAM(10) = NUMOPB, IPARAM(11) = NUMREO,
    c          OUTPUT: NUMOP  = total number of OP*x operations,
    c                  NUMOPB = total number of B*x operations if BMAT='G',
    c                  NUMREO = total number of steps of re-orthogonalization.
    c
    c  IPNTR   Integer array of length 14.  (OUTPUT)
    c          Pointer to mark the starting locations in the WORKD and WORKL
    c          arrays for matrices/vectors used by the Arnoldi iteration.
    c          -------------------------------------------------------------
    c          IPNTR(1): pointer to the current operand vector X in WORKD.
    c          IPNTR(2): pointer to the current result vector Y in WORKD.
    c          IPNTR(3): pointer to the vector B * X in WORKD when used in 
    c                    the shift-and-invert mode.
    c          IPNTR(4): pointer to the next available location in WORKL
    c                    that is untouched by the program.
    c          IPNTR(5): pointer to the NCV by NCV upper Hessenberg
    c                    matrix H in WORKL.
    c          IPNTR(6): pointer to the  ritz value array  RITZ
    c          IPNTR(7): pointer to the (projected) ritz vector array Q
    c          IPNTR(8): pointer to the error BOUNDS array in WORKL.
    c          IPNTR(14): pointer to the NP shifts in WORKL. See Remark 5 below.
    c
    c          Note: IPNTR(9:13) is only referenced by cneupd. See Remark 2 below.
    c
    c          IPNTR(9): pointer to the NCV RITZ values of the 
    c                    original system.
    c          IPNTR(10): Not Used
    c          IPNTR(11): pointer to the NCV corresponding error bounds.
    c          IPNTR(12): pointer to the NCV by NCV upper triangular
    c                     Schur matrix for H.
    c          IPNTR(13): pointer to the NCV by NCV matrix of eigenvectors
    c                     of the upper Hessenberg matrix H. Only referenced by
    c                     cneupd if RVEC = .TRUE. See Remark 2 below.
    c
    c          -------------------------------------------------------------
    c          
    c  WORKD   Complex  work array of length 3*N.  (REVERSE COMMUNICATION)
    c          Distributed array to be used in the basic Arnoldi iteration
    c          for reverse communication.  The user should not use WORKD 
    c          as temporary workspace during the iteration !!!!!!!!!!
    c          See Data Distribution Note below.  
    c
    c  WORKL   Complex  work array of length LWORKL.  (OUTPUT/WORKSPACE)
    c          Private (replicated) array on each PE or array allocated on
    c          the front end.  See Data Distribution Note below.
    c
    c  LWORKL  Integer.  (INPUT)
    c          LWORKL must be at least 3*NCV**2 + 5*NCV.
    c
    c  RWORK   Real   work array of length NCV (WORKSPACE)
    c          Private (replicated) array on each PE or array allocated on
    c          the front end.
    c
    c
    c  INFO    Integer.  (INPUT/OUTPUT)
    c          If INFO .EQ. 0, a randomly initial residual vector is used.
    c          If INFO .NE. 0, RESID contains the initial residual vector,
    c                          possibly from a previous run.
    c          Error flag on output.
    c          =  0: Normal exit.
    c          =  1: Maximum number of iterations taken.
    c                All possible eigenvalues of OP has been found. IPARAM(5)  
    c                returns the number of wanted converged Ritz values.
    c          =  2: No longer an informational error. Deprecated starting
    c                with release 2 of ARPACK.
    c          =  3: No shifts could be applied during a cycle of the 
    c                Implicitly restarted Arnoldi iteration. One possibility 
    c                is to increase the size of NCV relative to NEV. 
    c                See remark 4 below.
    c          = -1: N must be positive.
    c          = -2: NEV must be positive.
    c          = -3: NCV-NEV >= 2 and less than or equal to N.
    c          = -4: The maximum number of Arnoldi update iteration 
    c                must be greater than zero.
    c          = -5: WHICH must be one of 'LM', 'SM', 'LR', 'SR', 'LI', 'SI'
    c          = -6: BMAT must be one of 'I' or 'G'.
    c          = -7: Length of private work array is not sufficient.
    c          = -8: Error return from LAPACK eigenvalue calculation;
    c          = -9: Starting vector is zero.
    c          = -10: IPARAM(7) must be 1,2,3.
    c          = -11: IPARAM(7) = 1 and BMAT = 'G' are incompatible.
    c          = -12: IPARAM(1) must be equal to 0 or 1.
    c          = -9999: Could not build an Arnoldi factorization.
    c                   User input error highly likely.  Please
    c                   check actual array dimensions and layout.
    c                   IPARAM(5) returns the size of the current Arnoldi
    c                   factorization.
    c
    c\Remarks
    c  1. The computed Ritz values are approximate eigenvalues of OP. The
    c     selection of WHICH should be made with this in mind when using
    c     Mode = 3.  When operating in Mode = 3 setting WHICH = 'LM' will
    c     compute the NEV eigenvalues of the original problem that are
    c     closest to the shift SIGMA . After convergence, approximate eigenvalues 
    c     of the original problem may be obtained with the ARPACK subroutine cneupd.
    c
    c  2. If a basis for the invariant subspace corresponding to the converged Ritz 
    c     values is needed, the user must call cneupd immediately following 
    c     completion of cnaupd. This is new starting with release 2 of ARPACK.
    c
    c  3. If M can be factored into a Cholesky factorization M = LL`
    c     then Mode = 2 should not be selected.  Instead one should use
    c     Mode = 1 with  OP = inv(L)*A*inv(L`).  Appropriate triangular 
    c     linear systems should be solved with L and L` rather
    c     than computing inverses.  After convergence, an approximate
    c     eigenvector z of the original problem is recovered by solving
    c     L`z = x  where x is a Ritz vector of OP.
    c
    c  4. At present there is no a-priori analysis to guide the selection
    c     of NCV relative to NEV.  The only formal requirement is that NCV > NEV + 1.
    c     However, it is recommended that NCV .ge. 2*NEV.  If many problems of
    c     the same type are to be solved, one should experiment with increasing
    c     NCV while keeping NEV fixed for a given test problem.  This will 
    c     usually decrease the required number of OP*x operations but it
    c     also increases the work and storage required to maintain the orthogonal
    c     basis vectors.  The optimal "cross-over" with respect to CPU time
    c     is problem dependent and must be determined empirically. 
    c     See Chapter 8 of Reference 2 for further information.
    c
    c  5. When IPARAM(1) = 0, and IDO = 3, the user needs to provide the
    c     NP = IPARAM(8) complex shifts in locations
    c     WORKL(IPNTR(14)), WORKL(IPNTR(14)+1), ... , WORKL(IPNTR(14)+NP).
    c     Eigenvalues of the current upper Hessenberg matrix are located in
    c     WORKL(IPNTR(6)) through WORKL(IPNTR(6)+NCV-1). They are ordered
    c     according to the order defined by WHICH.  The associated Ritz estimates
    c     are located in WORKL(IPNTR(8)), WORKL(IPNTR(8)+1), ... ,
    c     WORKL(IPNTR(8)+NCV-1).
  */
  
  void cnaupd_(int* ido, char const* bmat, int* n, char const* which,
               int* nev, float* tol, std::complex<float>* resid, int* ncv,
               std::complex<float>* v, int* ldv, int* iparam, int* ipntr,
               std::complex<float> * workd, std::complex<float> * workl, int* lworkl,
               float* rwork, int* info);
  
  /*
    c\Name: cneupd 
    c 
    c\Description: 
    c  This subroutine returns the converged approximations to eigenvalues 
    c  of A*z = lambda*B*z and (optionally): 
    c 
    c      (1) The corresponding approximate eigenvectors; 
    c 
    c      (2) An orthonormal basis for the associated approximate 
    c          invariant subspace; 
    c 
    c      (3) Both.  
    c
    c  There is negligible additional cost to obtain eigenvectors.  An orthonormal 
    c  basis is always computed.  There is an additional storage cost of n*nev
    c  if both are requested (in this case a separate array Z must be supplied). 
    c
    c  The approximate eigenvalues and eigenvectors of  A*z = lambda*B*z
    c  are derived from approximate eigenvalues and eigenvectors of
    c  of the linear operator OP prescribed by the MODE selection in the
    c  call to CNAUPD.  CNAUPD must be called before this routine is called.
    c  These approximate eigenvalues and vectors are commonly called Ritz
    c  values and Ritz vectors respectively.  They are referred to as such 
    c  in the comments that follow.   The computed orthonormal basis for the 
    c  invariant subspace corresponding to these Ritz values is referred to as a 
    c  Schur basis. 
    c 
    c  The definition of OP as well as other terms and the relation of computed
    c  Ritz values and vectors of OP with respect to the given problem
    c  A*z = lambda*B*z may be found in the header of CNAUPD.  For a brief 
    c  description, see definitions of IPARAM(7), MODE and WHICH in the
    c  documentation of CNAUPD.
    c
    c\Usage:
    c  call cneupd 
    c     ( RVEC, HOWMNY, SELECT, D, Z, LDZ, SIGMA, WORKEV, BMAT, 
    c       N, WHICH, NEV, TOL, RESID, NCV, V, LDV, IPARAM, IPNTR, WORKD, 
    c       WORKL, LWORKL, RWORK, INFO )
    c
    c\Arguments:
    c  RVEC    LOGICAL  (INPUT)
    c          Specifies whether a basis for the invariant subspace corresponding
    c          to the converged Ritz value approximations for the eigenproblem 
    c          A*z = lambda*B*z is computed.
    c
    c             RVEC = .FALSE.     Compute Ritz values only.
    c
    c             RVEC = .TRUE.      Compute Ritz vectors or Schur vectors.
    c                                See Remarks below.
    c
    c  HOWMNY  Character*1  (INPUT)
    c          Specifies the form of the basis for the invariant subspace 
    c          corresponding to the converged Ritz values that is to be computed.
    c
    c          = 'A': Compute NEV Ritz vectors;
    c          = 'P': Compute NEV Schur vectors;
    c          = 'S': compute some of the Ritz vectors, specified
    c                 by the logical array SELECT.
    c
    c  SELECT  Logical array of dimension NCV.  (INPUT)
    c          If HOWMNY = 'S', SELECT specifies the Ritz vectors to be
    c          computed. To select the  Ritz vector corresponding to a
    c          Ritz value D(j), SELECT(j) must be set to .TRUE.. 
    c          If HOWMNY = 'A' or 'P', SELECT need not be initialized 
    c          but it is used as internal workspace.
    c
    c  D       Complex array of dimension NEV+1.  (OUTPUT)
    c          On exit, D contains the  Ritz  approximations 
    c          to the eigenvalues lambda for A*z = lambda*B*z.
    c
    c  Z       Complex N by NEV array    (OUTPUT)
    c          On exit, if RVEC = .TRUE. and HOWMNY = 'A', then the columns of 
    c          Z represents approximate eigenvectors (Ritz vectors) corresponding 
    c          to the NCONV=IPARAM(5) Ritz values for eigensystem
    c          A*z = lambda*B*z.
    c
    c          If RVEC = .FALSE. or HOWMNY = 'P', then Z is NOT REFERENCED.
    c
    c          NOTE: If if RVEC = .TRUE. and a Schur basis is not required, 
    c          the array Z may be set equal to first NEV+1 columns of the Arnoldi 
    c          basis array V computed by CNAUPD.  In this case the Arnoldi basis 
    c          will be destroyed and overwritten with the eigenvector basis.
    c
    c  LDZ     Integer.  (INPUT)
    c          The leading dimension of the array Z.  If Ritz vectors are
    c          desired, then  LDZ .ge.  max( 1, N ) is required.  
    c          In any case,  LDZ .ge. 1 is required.
    c
    c  SIGMA   Complex  (INPUT)
    c          If IPARAM(7) = 3 then SIGMA represents the shift. 
    c          Not referenced if IPARAM(7) = 1 or 2.
    c
    c  WORKEV  Complex work array of dimension 2*NCV.  (WORKSPACE)
    c
    c  **** The remaining arguments MUST be the same as for the   ****
    c  **** call to CNAUPD that was just completed.               ****
    c
    c  NOTE: The remaining arguments 
    c
    c           BMAT, N, WHICH, NEV, TOL, RESID, NCV, V, LDV, IPARAM, IPNTR, 
    c           WORKD, WORKL, LWORKL, RWORK, INFO 
    c
    c         must be passed directly to CNEUPD following the last call 
    c         to CNAUPD.  These arguments MUST NOT BE MODIFIED between
    c         the the last call to CNAUPD and the call to CNEUPD.
    c
    c  Three of these parameters (V, WORKL and INFO) are also output parameters:
    c
    c  V       Complex N by NCV array.  (INPUT/OUTPUT)
    c
    c          Upon INPUT: the NCV columns of V contain the Arnoldi basis
    c                      vectors for OP as constructed by CNAUPD .
    c
    c          Upon OUTPUT: If RVEC = .TRUE. the first NCONV=IPARAM(5) columns
    c                       contain approximate Schur vectors that span the
    c                       desired invariant subspace.
    c
    c          NOTE: If the array Z has been set equal to first NEV+1 columns
    c          of the array V and RVEC=.TRUE. and HOWMNY= 'A', then the
    c          Arnoldi basis held by V has been overwritten by the desired
    c          Ritz vectors.  If a separate array Z has been passed then
    c          the first NCONV=IPARAM(5) columns of V will contain approximate
    c          Schur vectors that span the desired invariant subspace.
    c
    c  WORKL   Real work array of length LWORKL.  (OUTPUT/WORKSPACE)
    c          WORKL(1:ncv*ncv+2*ncv) contains information obtained in
    c          cnaupd.  They are not changed by cneupd.
    c          WORKL(ncv*ncv+2*ncv+1:3*ncv*ncv+4*ncv) holds the
    c          untransformed Ritz values, the untransformed error estimates of 
    c          the Ritz values, the upper triangular matrix for H, and the
    c          associated matrix representation of the invariant subspace for H.
    c
    c          Note: IPNTR(9:13) contains the pointer into WORKL for addresses
    c          of the above information computed by cneupd.
    c          -------------------------------------------------------------
    c          IPNTR(9):  pointer to the NCV RITZ values of the
    c                     original system.
    c          IPNTR(10): Not used
    c          IPNTR(11): pointer to the NCV corresponding error estimates.
    c          IPNTR(12): pointer to the NCV by NCV upper triangular
    c                     Schur matrix for H.
    c          IPNTR(13): pointer to the NCV by NCV matrix of eigenvectors
    c                     of the upper Hessenberg matrix H. Only referenced by
    c                     cneupd if RVEC = .TRUE. See Remark 2 below.
    c          -------------------------------------------------------------
    c
    c  INFO    Integer.  (OUTPUT)
    c          Error flag on output.
    c          =  0: Normal exit.
    c
    c          =  1: The Schur form computed by LAPACK routine csheqr
    c                could not be reordered by LAPACK routine ctrsen.
    c                Re-enter subroutine cneupd with IPARAM(5)=NCV and
    c                increase the size of the array D to have
    c                dimension at least dimension NCV and allocate at least NCV
    c                columns for Z. NOTE: Not necessary if Z and V share
    c                the same space. Please notify the authors if this error
    c                occurs.
    c
    c          = -1: N must be positive.
    c          = -2: NEV must be positive.
    c          = -3: NCV-NEV >= 2 and less than or equal to N.
    c          = -5: WHICH must be one of 'LM', 'SM', 'LR', 'SR', 'LI', 'SI'
    c          = -6: BMAT must be one of 'I' or 'G'.
    c          = -7: Length of private work WORKL array is not sufficient.
    c          = -8: Error return from LAPACK eigenvalue calculation.
    c                This should never happened.
    c          = -9: Error return from calculation of eigenvectors.
    c                Informational error from LAPACK routine ctrevc.
    c          = -10: IPARAM(7) must be 1,2,3
    c          = -11: IPARAM(7) = 1 and BMAT = 'G' are incompatible.
    c          = -12: HOWMNY = 'S' not yet implemented
    c          = -13: HOWMNY must be one of 'A' or 'P' if RVEC = .true.
    c          = -14: CNAUPD did not find any eigenvalues to sufficient
    c                 accuracy.
    c          = -15: CNEUPD got a different count of the number of converged
    c                 Ritz values than CNAUPD got.  This indicates the user
    c                 probably made an error in passing data from CNAUPD to
    c                 CNEUPD or that the data was modified before entering
    c                 CNEUPD
  */
  
  void cneupd_(bool* rvec, char const* howmny, int const* select,
                std::complex<float>* d, std::complex<float>* z, int* ldz,
                std::complex<float> * sigma, std::complex<float>* workev,
                char const* bmat, int* n, char const* which, int* nev,
                float* tol, std::complex<float>* resid, int* ncv, std::complex<float>* v,
                int* ldv, int* iparam, int* ipntr, std::complex<float>* workd,
                std::complex<float>* workl, int* lworkl, float* rwork, int* info);

  /*
    c\Name: dnaupd
    c
    c\Description:
    c  Reverse communication interface for the Implicitly Restarted Arnoldi
    c  iteration. This subroutine computes approximations to a few eigenpairs
    c  of a linear operator "OP" with respect to a semi-inner product defined by
    c  a symmetric positive semi-definite real matrix B. B may be the identity
    c  matrix. NOTE: If the linear operator "OP" is real and symmetric
    c  with respect to the real positive semi-definite symmetric matrix B,
    c  i.e. B*OP = (OP`)*B, then subroutine dsaupd  should be used instead.
    c
    c  The computed approximate eigenvalues are called Ritz values and
    c  the corresponding approximate eigenvectors are called Ritz vectors.
    c
    c  dnaupd  is usually called iteratively to solve one of the
    c  following problems:
    c
    c  Mode 1:  A*x = lambda*x.
    c           ===> OP = A  and  B = I.
    c
    c  Mode 2:  A*x = lambda*M*x, M symmetric positive definite
    c           ===> OP = inv[M]*A  and  B = M.
    c           ===> (If M can be factored see remark 3 below)
    c
    c  Mode 3:  A*x = lambda*M*x, M symmetric semi-definite
    c           ===> OP = Real_Part{ inv[A - sigma*M]*M }  and  B = M.
    c           ===> shift-and-invert mode (in real arithmetic)
    c           If OP*x = amu*x, then
    c           amu = 1/2 * [ 1/(lambda-sigma) + 1/(lambda-conjg(sigma)) ].
    c           Note: If sigma is real, i.e. imaginary part of sigma is zero;
    c                 Real_Part{ inv[A - sigma*M]*M } == inv[A - sigma*M]*M
    c                 amu == 1/(lambda-sigma).
    c
    c  Mode 4:  A*x = lambda*M*x, M symmetric semi-definite
    c           ===> OP = Imaginary_Part{ inv[A - sigma*M]*M }  and  B = M.
    c           ===> shift-and-invert mode (in real arithmetic)
    c           If OP*x = amu*x, then
    c           amu = 1/2i * [ 1/(lambda-sigma) - 1/(lambda-conjg(sigma)) ].
    c
    c  Both mode 3 and 4 give the same enhancement to eigenvalues close to
    c  the (complex) shift sigma.  However, as lambda goes to infinity,
    c  the operator OP in mode 4 dampens the eigenvalues more strongly than
    c  does OP defined in mode 3.
    c
    c  NOTE: The action of w <- inv[A - sigma*M]*v or w <- inv[M]*v
    c        should be accomplished either by a direct method
    c        using a sparse matrix factorization and solving
    c
    c           [A - sigma*M]*w = v  or M*w = v,
    c
    c        or through an iterative method for solving these
    c        systems.  If an iterative method is used, the
    c        convergence test must be more stringent than
    c        the accuracy requirements for the eigenvalue
    c        approximations.
    c
    c\Usage:
    c  call dnaupd
    c     ( IDO, BMAT, N, WHICH, NEV, TOL, RESID, NCV, V, LDV, IPARAM,
    c       IPNTR, WORKD, WORKL, LWORKL, INFO )
    c
    c\Arguments
    c  IDO     Integer.  (INPUT/OUTPUT)
    c          Reverse communication flag.  IDO must be zero on the first
    c          call to dnaupd .  IDO will be set internally to
    c          indicate the type of operation to be performed.  Control is
    c          then given back to the calling routine which has the
    c          responsibility to carry out the requested operation and call
    c          dnaupd  with the result.  The operand is given in
    c          WORKD(IPNTR(1)), the result must be put in WORKD(IPNTR(2)).
    c          -------------------------------------------------------------
    c          IDO =  0: first call to the reverse communication interface
    c          IDO = -1: compute  Y = OP * X  where
    c                    IPNTR(1) is the pointer into WORKD for X,
    c                    IPNTR(2) is the pointer into WORKD for Y.
    c                    This is for the initialization phase to force the
    c                    starting vector into the range of OP.
    c          IDO =  1: compute  Y = OP * X  where
    c                    IPNTR(1) is the pointer into WORKD for X,
    c                    IPNTR(2) is the pointer into WORKD for Y.
    c                    In mode 3 and 4, the vector B * X is already
    c                    available in WORKD(ipntr(3)).  It does not
    c                    need to be recomputed in forming OP * X.
    c          IDO =  2: compute  Y = B * X  where
    c                    IPNTR(1) is the pointer into WORKD for X,
    c                    IPNTR(2) is the pointer into WORKD for Y.
    c          IDO =  3: compute the IPARAM(8) real and imaginary parts
    c                    of the shifts where INPTR(14) is the pointer
    c                    into WORKL for placing the shifts. See Remark
    c                    5 below.
    c          IDO = 99: done
    c          -------------------------------------------------------------
    c
    c  BMAT    Character*1.  (INPUT)
    c          BMAT specifies the type of the matrix B that defines the
    c          semi-inner product for the operator OP.
    c          BMAT = 'I' -> standard eigenvalue problem A*x = lambda*x
    c          BMAT = 'G' -> generalized eigenvalue problem A*x = lambda*B*x
    c
    c  N       Integer.  (INPUT)
    c          Dimension of the eigenproblem.
    c
    c  WHICH   Character*2.  (INPUT)
    c          'LM' -> want the NEV eigenvalues of largest magnitude.
    c          'SM' -> want the NEV eigenvalues of smallest magnitude.
    c          'LR' -> want the NEV eigenvalues of largest real part.
    c          'SR' -> want the NEV eigenvalues of smallest real part.
    c          'LI' -> want the NEV eigenvalues of largest imaginary part.
    c          'SI' -> want the NEV eigenvalues of smallest imaginary part.
    c
    c  NEV     Integer.  (INPUT)
    c          Number of eigenvalues of OP to be computed. 0 < NEV < N-1.
    c
    c  TOL     Double precision  scalar.  (INPUT/OUTPUT)
    c          Stopping criterion: the relative accuracy of the Ritz value
    c          is considered acceptable if BOUNDS(I) .LE. TOL*ABS(RITZ(I))
    c          where ABS(RITZ(I)) is the magnitude when RITZ(I) is complex.
    c          DEFAULT = DLAMCH ('EPS')  (machine precision as computed
    c                    by the LAPACK auxiliary subroutine DLAMCH ).
    c
    c  RESID   Double precision  array of length N.  (INPUT/OUTPUT)
    c          On INPUT:
    c          If INFO .EQ. 0, a random initial residual vector is used.
    c          If INFO .NE. 0, RESID contains the initial residual vector,
    c                          possibly from a previous run.
    c          On OUTPUT:
    c          RESID contains the final residual vector.
    c
    c  NCV     Integer.  (INPUT)
    c          Number of columns of the matrix V. NCV must satisfy the two
    c          inequalities 2 <= NCV-NEV and NCV <= N.
    c          This will indicate how many Arnoldi vectors are generated
    c          at each iteration.  After the startup phase in which NEV
    c          Arnoldi vectors are generated, the algorithm generates
    c          approximately NCV-NEV Arnoldi vectors at each subsequent update
    c          iteration. Most of the cost in generating each Arnoldi vector is
    c          in the matrix-vector operation OP*x.
    c          NOTE: 2 <= NCV-NEV in order that complex conjugate pairs of Ritz
    c          values are kept together. (See remark 4 below)
    c
    c  V       Double precision  array N by NCV.  (OUTPUT)
    c          Contains the final set of Arnoldi basis vectors.
    c
    c  LDV     Integer.  (INPUT)
    c          Leading dimension of V exactly as declared in the calling program.
    c
    c  IPARAM  Integer array of length 11.  (INPUT/OUTPUT)
    c          IPARAM(1) = ISHIFT: method for selecting the implicit shifts.
    c          The shifts selected at each iteration are used to restart
    c          the Arnoldi iteration in an implicit fashion.
    c          -------------------------------------------------------------
    c          ISHIFT = 0: the shifts are provided by the user via
    c                      reverse communication.  The real and imaginary
    c                      parts of the NCV eigenvalues of the Hessenberg
    c                      matrix H are returned in the part of the WORKL
    c                      array corresponding to RITZR and RITZI. See remark
    c                      5 below.
    c          ISHIFT = 1: exact shifts with respect to the current
    c                      Hessenberg matrix H.  This is equivalent to
    c                      restarting the iteration with a starting vector
    c                      that is a linear combination of approximate Schur
    c                      vectors associated with the "wanted" Ritz values.
    c          -------------------------------------------------------------
    c
    c          IPARAM(2) = No longer referenced.
    c
    c          IPARAM(3) = MXITER
    c          On INPUT:  maximum number of Arnoldi update iterations allowed.
    c          On OUTPUT: actual number of Arnoldi update iterations taken.
    c
    c          IPARAM(4) = NB: blocksize to be used in the recurrence.
    c          The code currently works only for NB = 1.
    c
    c          IPARAM(5) = NCONV: number of "converged" Ritz values.
    c          This represents the number of Ritz values that satisfy
    c          the convergence criterion.
    c
    c          IPARAM(6) = IUPD
    c          No longer referenced. Implicit restarting is ALWAYS used.
    c
    c          IPARAM(7) = MODE
    c          On INPUT determines what type of eigenproblem is being solved.
    c          Must be 1,2,3,4; See under \Description of dnaupd  for the
    c          four modes available.
    c
    c          IPARAM(8) = NP
    c          When ido = 3 and the user provides shifts through reverse
    c          communication (IPARAM(1)=0), dnaupd  returns NP, the number
    c          of shifts the user is to provide. 0 < NP <=NCV-NEV. See Remark
    c          5 below.
    c
    c          IPARAM(9) = NUMOP, IPARAM(10) = NUMOPB, IPARAM(11) = NUMREO,
    c          OUTPUT: NUMOP  = total number of OP*x operations,
    c                  NUMOPB = total number of B*x operations if BMAT='G',
    c                  NUMREO = total number of steps of re-orthogonalization.
    c
    c  IPNTR   Integer array of length 14.  (OUTPUT)
    c          Pointer to mark the starting locations in the WORKD and WORKL
    c          arrays for matrices/vectors used by the Arnoldi iteration.
    c          -------------------------------------------------------------
    c          IPNTR(1): pointer to the current operand vector X in WORKD.
    c          IPNTR(2): pointer to the current result vector Y in WORKD.
    c          IPNTR(3): pointer to the vector B * X in WORKD when used in
    c                    the shift-and-invert mode.
    c          IPNTR(4): pointer to the next available location in WORKL
    c                    that is untouched by the program.
    c          IPNTR(5): pointer to the NCV by NCV upper Hessenberg matrix
    c                    H in WORKL.
    c          IPNTR(6): pointer to the real part of the ritz value array
    c                    RITZR in WORKL.
    c          IPNTR(7): pointer to the imaginary part of the ritz value array
    c                    RITZI in WORKL.
    c          IPNTR(8): pointer to the Ritz estimates in array WORKL associated
    c                    with the Ritz values located in RITZR and RITZI in WORKL.
    c
    c          IPNTR(14): pointer to the NP shifts in WORKL. See Remark 5 below.
    c
    c          Note: IPNTR(9:13) is only referenced by dneupd . See Remark 2 below.
    c
    c          IPNTR(9):  pointer to the real part of the NCV RITZ values of the
    c                     original system.
    c          IPNTR(10): pointer to the imaginary part of the NCV RITZ values of
    c                     the original system.
    c          IPNTR(11): pointer to the NCV corresponding error bounds.
    c          IPNTR(12): pointer to the NCV by NCV upper quasi-triangular
    c                     Schur matrix for H.
    c          IPNTR(13): pointer to the NCV by NCV matrix of eigenvectors
    c                     of the upper Hessenberg matrix H. Only referenced by
    c                     dneupd  if RVEC = .TRUE. See Remark 2 below.
    c          -------------------------------------------------------------
    c
    c  WORKD   Double precision  work array of length 3*N.  (REVERSE COMMUNICATION)
    c          Distributed array to be used in the basic Arnoldi iteration
    c          for reverse communication.  The user should not use WORKD
    c          as temporary workspace during the iteration. Upon termination
    c          WORKD(1:N) contains B*RESID(1:N). If an invariant subspace
    c          associated with the converged Ritz values is desired, see remark
    c          2 below, subroutine dneupd  uses this output.
    c          See Data Distribution Note below.
    c
    c  WORKL   Double precision  work array of length LWORKL.  (OUTPUT/WORKSPACE)
    c          Private (replicated) array on each PE or array allocated on
    c          the front end.  See Data Distribution Note below.
    c
    c  LWORKL  Integer.  (INPUT)
    c          LWORKL must be at least 3*NCV**2 + 6*NCV.
    c
    c  INFO    Integer.  (INPUT/OUTPUT)
    c          If INFO .EQ. 0, a randomly initial residual vector is used.
    c          If INFO .NE. 0, RESID contains the initial residual vector,
    c                          possibly from a previous run.
    c          Error flag on output.
    c          =  0: Normal exit.
    c          =  1: Maximum number of iterations taken.
    c                All possible eigenvalues of OP has been found. IPARAM(5)
    c                returns the number of wanted converged Ritz values.
    c          =  2: No longer an informational error. Deprecated starting
    c                with release 2 of ARPACK.
    c          =  3: No shifts could be applied during a cycle of the
    c                Implicitly restarted Arnoldi iteration. One possibility
    c                is to increase the size of NCV relative to NEV.
    c                See remark 4 below.
    c          = -1: N must be positive.
    c          = -2: NEV must be positive.
    c          = -3: NCV-NEV >= 2 and less than or equal to N.
    c          = -4: The maximum number of Arnoldi update iteration
    c                must be greater than zero.
    c          = -5: WHICH must be one of 'LM', 'SM', 'LR', 'SR', 'LI', 'SI'
    c          = -6: BMAT must be one of 'I' or 'G'.
    c          = -7: Length of private work array is not sufficient.
    c          = -8: Error return from LAPACK eigenvalue calculation;
    c          = -9: Starting vector is zero.
    c          = -10: IPARAM(7) must be 1,2,3,4.
    c          = -11: IPARAM(7) = 1 and BMAT = 'G' are incompatible.
    c          = -12: IPARAM(1) must be equal to 0 or 1.
    c          = -9999: Could not build an Arnoldi factorization.
    c                   IPARAM(5) returns the size of the current Arnoldi
    c                   factorization.
  
  */
  
  void dnaupd_(int* ido, char const* bmat, int* n, char const* which, int* nev,
               double* tol, double* resid, int* ncv, double* v, int* ldv,
               int* iparam, int* ipntr, double* workd, double* workl,
               int* lworkl, int* info);
  
  /*
    c\Name: dneupd 
    c
    c\Description: 
    c
    c  This subroutine returns the converged approximations to eigenvalues
    c  of A*z = lambda*B*z and (optionally):
    c
    c      (1) The corresponding approximate eigenvectors;
    c
    c      (2) An orthonormal basis for the associated approximate
    c          invariant subspace;
    c
    c      (3) Both.
    c
    c  There is negligible additional cost to obtain eigenvectors.  An orthonormal
    c  basis is always computed.  There is an additional storage cost of n*nev
    c  if both are requested (in this case a separate array Z must be supplied).
    c
    c  The approximate eigenvalues and eigenvectors of  A*z = lambda*B*z
    c  are derived from approximate eigenvalues and eigenvectors of
    c  of the linear operator OP prescribed by the MODE selection in the
    c  call to DNAUPD .  DNAUPD  must be called before this routine is called.
    c  These approximate eigenvalues and vectors are commonly called Ritz
    c  values and Ritz vectors respectively.  They are referred to as such
    c  in the comments that follow.  The computed orthonormal basis for the
    c  invariant subspace corresponding to these Ritz values is referred to as a
    c  Schur basis.
    c
    c  See documentation in the header of the subroutine DNAUPD  for 
    c  definition of OP as well as other terms and the relation of computed
    c  Ritz values and Ritz vectors of OP with respect to the given problem
    c  A*z = lambda*B*z.  For a brief description, see definitions of 
    c  IPARAM(7), MODE and WHICH in the documentation of DNAUPD .
    c
    c\Usage:
    c  call dneupd  
    c     ( RVEC, HOWMNY, SELECT, DR, DI, Z, LDZ, SIGMAR, SIGMAI, WORKEV, BMAT, 
    c       N, WHICH, NEV, TOL, RESID, NCV, V, LDV, IPARAM, IPNTR, WORKD, WORKL, 
    c       LWORKL, INFO )
    c
    c\Arguments:
    c  RVEC    LOGICAL  (INPUT) 
    c          Specifies whether a basis for the invariant subspace corresponding 
    c          to the converged Ritz value approximations for the eigenproblem 
    c          A*z = lambda*B*z is computed.
    c
    c             RVEC = .FALSE.     Compute Ritz values only.
    c
    c             RVEC = .TRUE.      Compute the Ritz vectors or Schur vectors.
    c                                See Remarks below. 
    c 
    c  HOWMNY  Character*1  (INPUT) 
    c          Specifies the form of the basis for the invariant subspace 
    c          corresponding to the converged Ritz values that is to be computed.
    c
    c          = 'A': Compute NEV Ritz vectors; 
    c          = 'P': Compute NEV Schur vectors;
    c          = 'S': compute some of the Ritz vectors, specified
    c                 by the logical array SELECT.
    c
    c  SELECT  Logical array of dimension NCV.  (INPUT)
    c          If HOWMNY = 'S', SELECT specifies the Ritz vectors to be
    c          computed. To select the Ritz vector corresponding to a
    c          Ritz value (DR(j), DI(j)), SELECT(j) must be set to .TRUE.. 
    c          If HOWMNY = 'A' or 'P', SELECT is used as internal workspace.
    c
    c  DR      Double precision  array of dimension NEV+1.  (OUTPUT)
    c          If IPARAM(7) = 1,2 or 3 and SIGMAI=0.0  then on exit: DR contains 
    c          the real part of the Ritz  approximations to the eigenvalues of 
    c          A*z = lambda*B*z. 
    c          If IPARAM(7) = 3, 4 and SIGMAI is not equal to zero, then on exit:
    c          DR contains the real part of the Ritz values of OP computed by 
    c          DNAUPD . A further computation must be performed by the user
    c          to transform the Ritz values computed for OP by DNAUPD  to those
    c          of the original system A*z = lambda*B*z. See remark 3 below.
    c
    c  DI      Double precision  array of dimension NEV+1.  (OUTPUT)
    c          On exit, DI contains the imaginary part of the Ritz value 
    c          approximations to the eigenvalues of A*z = lambda*B*z associated
    c          with DR.
    c
    c          NOTE: When Ritz values are complex, they will come in complex 
    c                conjugate pairs.  If eigenvectors are requested, the 
    c                corresponding Ritz vectors will also come in conjugate 
    c                pairs and the real and imaginary parts of these are 
    c                represented in two consecutive columns of the array Z 
    c                (see below).
    c
    c  Z       Double precision  N by NEV+1 array if RVEC = .TRUE. and HOWMNY = 'A'. (OUTPUT)
    c          On exit, if RVEC = .TRUE. and HOWMNY = 'A', then the columns of 
    c          Z represent approximate eigenvectors (Ritz vectors) corresponding 
    c          to the NCONV=IPARAM(5) Ritz values for eigensystem 
    c          A*z = lambda*B*z. 
    c 
    c          The complex Ritz vector associated with the Ritz value 
    c          with positive imaginary part is stored in two consecutive 
    c          columns.  The first column holds the real part of the Ritz 
    c          vector and the second column holds the imaginary part.  The 
    c          Ritz vector associated with the Ritz value with negative 
    c          imaginary part is simply the complex conjugate of the Ritz vector 
    c          associated with the positive imaginary part.
    c
    c          If  RVEC = .FALSE. or HOWMNY = 'P', then Z is not referenced.
    c
    c          NOTE: If if RVEC = .TRUE. and a Schur basis is not required,
    c          the array Z may be set equal to first NEV+1 columns of the Arnoldi
    c          basis array V computed by DNAUPD .  In this case the Arnoldi basis
    c          will be destroyed and overwritten with the eigenvector basis.
    c
    c  LDZ     Integer.  (INPUT)
    c          The leading dimension of the array Z.  If Ritz vectors are
    c          desired, then  LDZ >= max( 1, N ).  In any case,  LDZ >= 1.
    c
    c  SIGMAR  Double precision   (INPUT)
    c          If IPARAM(7) = 3 or 4, represents the real part of the shift. 
    c          Not referenced if IPARAM(7) = 1 or 2.
    c
    c  SIGMAI  Double precision   (INPUT)
    c          If IPARAM(7) = 3 or 4, represents the imaginary part of the shift. 
    c          Not referenced if IPARAM(7) = 1 or 2. See remark 3 below.
    c
    c  WORKEV  Double precision  work array of dimension 3*NCV.  (WORKSPACE)
    c
    c  **** The remaining arguments MUST be the same as for the   ****
    c  **** call to DNAUPD  that was just completed.               ****
    c
    c  NOTE: The remaining arguments
    c
    c           BMAT, N, WHICH, NEV, TOL, RESID, NCV, V, LDV, IPARAM, IPNTR,
    c           WORKD, WORKL, LWORKL, INFO
    c
    c         must be passed directly to DNEUPD  following the last call
    c         to DNAUPD .  These arguments MUST NOT BE MODIFIED between
    c         the the last call to DNAUPD  and the call to DNEUPD .
    c
    c  Three of these parameters (V, WORKL, INFO) are also output parameters:
    c
    c  V       Double precision  N by NCV array.  (INPUT/OUTPUT)
    c
    c          Upon INPUT: the NCV columns of V contain the Arnoldi basis
    c                      vectors for OP as constructed by DNAUPD  .
    c
    c          Upon OUTPUT: If RVEC = .TRUE. the first NCONV=IPARAM(5) columns
    c                       contain approximate Schur vectors that span the
    c                       desired invariant subspace.  See Remark 2 below.
    c
    c          NOTE: If the array Z has been set equal to first NEV+1 columns
    c          of the array V and RVEC=.TRUE. and HOWMNY= 'A', then the
    c          Arnoldi basis held by V has been overwritten by the desired
    c          Ritz vectors.  If a separate array Z has been passed then
    c          the first NCONV=IPARAM(5) columns of V will contain approximate
    c          Schur vectors that span the desired invariant subspace.
    c
    c  WORKL   Double precision  work array of length LWORKL.  (OUTPUT/WORKSPACE)
    c          WORKL(1:ncv*ncv+3*ncv) contains information obtained in
    c          dnaupd .  They are not changed by dneupd .
    c          WORKL(ncv*ncv+3*ncv+1:3*ncv*ncv+6*ncv) holds the
    c          real and imaginary part of the untransformed Ritz values,
    c          the upper quasi-triangular matrix for H, and the
    c          associated matrix representation of the invariant subspace for H.
    c
    c          Note: IPNTR(9:13) contains the pointer into WORKL for addresses
    c          of the above information computed by dneupd .
    c          -------------------------------------------------------------
    c          IPNTR(9):  pointer to the real part of the NCV RITZ values of the
    c                     original system.
    c          IPNTR(10): pointer to the imaginary part of the NCV RITZ values of
    c                     the original system.
    c          IPNTR(11): pointer to the NCV corresponding error bounds.
    c          IPNTR(12): pointer to the NCV by NCV upper quasi-triangular
    c                     Schur matrix for H.
    c          IPNTR(13): pointer to the NCV by NCV matrix of eigenvectors
    c                     of the upper Hessenberg matrix H. Only referenced by
    c                     dneupd  if RVEC = .TRUE. See Remark 2 below.
    c          -------------------------------------------------------------
    c
    c  INFO    Integer.  (OUTPUT)
    c          Error flag on output.
    c
    c          =  0: Normal exit.
    c
    c          =  1: The Schur form computed by LAPACK routine dlahqr 
    c                could not be reordered by LAPACK routine dtrsen .
    c                Re-enter subroutine dneupd  with IPARAM(5)=NCV and 
    c                increase the size of the arrays DR and DI to have 
    c                dimension at least dimension NCV and allocate at least NCV 
    c                columns for Z. NOTE: Not necessary if Z and V share 
    c                the same space. Please notify the authors if this error
    c                occurs.
    c
    c          = -1: N must be positive.
    c          = -2: NEV must be positive.
    c          = -3: NCV-NEV >= 2 and less than or equal to N.
    c          = -5: WHICH must be one of 'LM', 'SM', 'LR', 'SR', 'LI', 'SI'
    c          = -6: BMAT must be one of 'I' or 'G'.
    c          = -7: Length of private work WORKL array is not sufficient.
    c          = -8: Error return from calculation of a real Schur form.
    c                Informational error from LAPACK routine dlahqr .
    c          = -9: Error return from calculation of eigenvectors.
    c                Informational error from LAPACK routine dtrevc .
    c          = -10: IPARAM(7) must be 1,2,3,4.
    c          = -11: IPARAM(7) = 1 and BMAT = 'G' are incompatible.
    c          = -12: HOWMNY = 'S' not yet implemented
    c          = -13: HOWMNY must be one of 'A' or 'P' if RVEC = .true.
    c          = -14: DNAUPD  did not find any eigenvalues to sufficient
    c                 accuracy.
    c          = -15: DNEUPD got a different count of the number of converged
    c                 Ritz values than DNAUPD got.  This indicates the user
    c                 probably made an error in passing data from DNAUPD to
    c                 DNEUPD or that the data was modified before entering
    c                 DNEUPD

  */
  
  void dneupd_(bool* rvec, char const* howmny, int const* select, double* dr,
                double* di, double* z, int* ldz, double* sigmar, double* sigmai,
                char const* bmat, int* n, char const* which, int* nev, double* tol,
                double* resid, int* ncv, double* v, int* ldv, int* iparam,
                int* ipntr, double* workd, double* workl, int* lworkl,
                int* info);
  
  void dsaupd_(int* ido, char const* bmat, int* n, char const* which, int* nev,
                double* tol, double* resid, int* ncv, double* v, int* ldv,
                int* iparam, int* ipntr, double* workd, double* workl,
                int* lworkl, int* info);
  
  void dseupd_(bool* rvec, char const* howmny, int const* select, double* d,
                double* z, int* ldz, double* sigma, char const* bmat, int* n,
                char const* which, int* nev, double* tol, double* resid, int* ncv,
                double* v, int* ldv, int* iparam, int* ipntr, double* workd,
                double* workl, int* lworkl, int* info);

  /*
    c\Name: snaupd
    c
    c\Description: 
    c  Reverse communication interface for the Implicitly Restarted Arnoldi
    c  iteration. This subroutine computes approximations to a few eigenpairs 
    c  of a linear operator "OP" with respect to a semi-inner product defined by 
    c  a symmetric positive semi-definite real matrix B. B may be the identity 
    c  matrix. NOTE: If the linear operator "OP" is real and symmetric 
    c  with respect to the real positive semi-definite symmetric matrix B, 
    c  i.e. B*OP = (OP`)*B, then subroutine ssaupd should be used instead.
    c
    c  The computed approximate eigenvalues are called Ritz values and
    c  the corresponding approximate eigenvectors are called Ritz vectors.
    c
    c  snaupd is usually called iteratively to solve one of the 
    c  following problems:
    c
    c  Mode 1:  A*x = lambda*x.
    c           ===> OP = A  and  B = I.
    c
    c  Mode 2:  A*x = lambda*M*x, M symmetric positive definite
    c           ===> OP = inv[M]*A  and  B = M.
    c           ===> (If M can be factored see remark 3 below)
    c
    c  Mode 3:  A*x = lambda*M*x, M symmetric semi-definite
    c           ===> OP = Real_Part{ inv[A - sigma*M]*M }  and  B = M. 
    c           ===> shift-and-invert mode (in real arithmetic)
    c           If OP*x = amu*x, then 
    c           amu = 1/2 * [ 1/(lambda-sigma) + 1/(lambda-conjg(sigma)) ].
    c           Note: If sigma is real, i.e. imaginary part of sigma is zero;
    c                 Real_Part{ inv[A - sigma*M]*M } == inv[A - sigma*M]*M 
    c                 amu == 1/(lambda-sigma). 
    c  
    c  Mode 4:  A*x = lambda*M*x, M symmetric semi-definite
    c           ===> OP = Imaginary_Part{ inv[A - sigma*M]*M }  and  B = M. 
    c           ===> shift-and-invert mode (in real arithmetic)
    c           If OP*x = amu*x, then 
    c           amu = 1/2i * [ 1/(lambda-sigma) - 1/(lambda-conjg(sigma)) ].
    c
    c  Both mode 3 and 4 give the same enhancement to eigenvalues close to
    c  the (complex) shift sigma.  However, as lambda goes to infinity,
    c  the operator OP in mode 4 dampens the eigenvalues more strongly than
    c  does OP defined in mode 3.
    c
    c  NOTE: The action of w <- inv[A - sigma*M]*v or w <- inv[M]*v
    c        should be accomplished either by a direct method
    c        using a sparse matrix factorization and solving
    c
    c           [A - sigma*M]*w = v  or M*w = v,
    c
    c        or through an iterative method for solving these
    c        systems.  If an iterative method is used, the
    c        convergence test must be more stringent than
    c        the accuracy requirements for the eigenvalue
    c        approximations.
    c
    c\Usage:
    c  call snaupd
    c     ( IDO, BMAT, N, WHICH, NEV, TOL, RESID, NCV, V, LDV, IPARAM,
    c       IPNTR, WORKD, WORKL, LWORKL, INFO )
    c
    c\Arguments
    c  IDO     Integer.  (INPUT/OUTPUT)
    c          Reverse communication flag.  IDO must be zero on the first 
    c          call to snaupd.  IDO will be set internally to
    c          indicate the type of operation to be performed.  Control is
    c          then given back to the calling routine which has the
    c          responsibility to carry out the requested operation and call
    c          snaupd with the result.  The operand is given in
    c          WORKD(IPNTR(1)), the result must be put in WORKD(IPNTR(2)).
    c          -------------------------------------------------------------
    c          IDO =  0: first call to the reverse communication interface
    c          IDO = -1: compute  Y = OP * X  where
    c                    IPNTR(1) is the pointer into WORKD for X,
    c                    IPNTR(2) is the pointer into WORKD for Y.
    c                    This is for the initialization phase to force the
    c                    starting vector into the range of OP.
    c          IDO =  1: compute  Y = OP * X  where
    c                    IPNTR(1) is the pointer into WORKD for X,
    c                    IPNTR(2) is the pointer into WORKD for Y.
    c                    In mode 3 and 4, the vector B * X is already
    c                    available in WORKD(ipntr(3)).  It does not
    c                    need to be recomputed in forming OP * X.
    c          IDO =  2: compute  Y = B * X  where
    c                    IPNTR(1) is the pointer into WORKD for X,
    c                    IPNTR(2) is the pointer into WORKD for Y.
    c          IDO =  3: compute the IPARAM(8) real and imaginary parts 
    c                    of the shifts where INPTR(14) is the pointer
    c                    into WORKL for placing the shifts. See Remark
    c                    5 below.
    c          IDO = 99: done
    c          -------------------------------------------------------------
    c             
    c  BMAT    Character*1.  (INPUT)
    c          BMAT specifies the type of the matrix B that defines the
    c          semi-inner product for the operator OP.
    c          BMAT = 'I' -> standard eigenvalue problem A*x = lambda*x
    c          BMAT = 'G' -> generalized eigenvalue problem A*x = lambda*B*x
    c
    c  N       Integer.  (INPUT)
    c          Dimension of the eigenproblem.
    c
    c  WHICH   Character*2.  (INPUT)
    c          'LM' -> want the NEV eigenvalues of largest magnitude.
    c          'SM' -> want the NEV eigenvalues of smallest magnitude.
    c          'LR' -> want the NEV eigenvalues of largest real part.
    c          'SR' -> want the NEV eigenvalues of smallest real part.
    c          'LI' -> want the NEV eigenvalues of largest imaginary part.
    c          'SI' -> want the NEV eigenvalues of smallest imaginary part.
    c
    c  NEV     Integer.  (INPUT)
    c          Number of eigenvalues of OP to be computed. 0 < NEV < N-1.
    c
    c  TOL     Real  scalar.  (INPUT)
    c          Stopping criterion: the relative accuracy of the Ritz value 
    c          is considered acceptable if BOUNDS(I) .LE. TOL*ABS(RITZ(I))
    c          where ABS(RITZ(I)) is the magnitude when RITZ(I) is complex.
    c          DEFAULT = SLAMCH('EPS')  (machine precision as computed
    c                    by the LAPACK auxiliary subroutine SLAMCH).
    c
    c  RESID   Real  array of length N.  (INPUT/OUTPUT)
    c          On INPUT: 
    c          If INFO .EQ. 0, a random initial residual vector is used.
    c          If INFO .NE. 0, RESID contains the initial residual vector,
    c                          possibly from a previous run.
    c          On OUTPUT:
    c          RESID contains the final residual vector.
    c
    c  NCV     Integer.  (INPUT)
    c          Number of columns of the matrix V. NCV must satisfy the two
    c          inequalities 2 <= NCV-NEV and NCV <= N.
    c          This will indicate how many Arnoldi vectors are generated 
    c          at each iteration.  After the startup phase in which NEV 
    c          Arnoldi vectors are generated, the algorithm generates 
    c          approximately NCV-NEV Arnoldi vectors at each subsequent update 
    c          iteration. Most of the cost in generating each Arnoldi vector is 
    c          in the matrix-vector operation OP*x. 
    c          NOTE: 2 <= NCV-NEV in order that complex conjugate pairs of Ritz 
    c          values are kept together. (See remark 4 below)
    c
    c  V       Real  array N by NCV.  (OUTPUT)
    c          Contains the final set of Arnoldi basis vectors. 
    c
    c  LDV     Integer.  (INPUT)
    c          Leading dimension of V exactly as declared in the calling program.
    c
    c  IPARAM  Integer array of length 11.  (INPUT/OUTPUT)
    c          IPARAM(1) = ISHIFT: method for selecting the implicit shifts.
    c          The shifts selected at each iteration are used to restart
    c          the Arnoldi iteration in an implicit fashion.
    c          -------------------------------------------------------------
    c          ISHIFT = 0: the shifts are provided by the user via
    c                      reverse communication.  The real and imaginary
    c                      parts of the NCV eigenvalues of the Hessenberg
    c                      matrix H are returned in the part of the WORKL 
    c                      array corresponding to RITZR and RITZI. See remark 
    c                      5 below.
    c          ISHIFT = 1: exact shifts with respect to the current
    c                      Hessenberg matrix H.  This is equivalent to 
    c                      restarting the iteration with a starting vector
    c                      that is a linear combination of approximate Schur
    c                      vectors associated with the "wanted" Ritz values.
    c          -------------------------------------------------------------
    c
    c          IPARAM(2) = No longer referenced.
    c
    c          IPARAM(3) = MXITER
    c          On INPUT:  maximum number of Arnoldi update iterations allowed. 
    c          On OUTPUT: actual number of Arnoldi update iterations taken. 
    c
    c          IPARAM(4) = NB: blocksize to be used in the recurrence.
    c          The code currently works only for NB = 1.
    c
    c          IPARAM(5) = NCONV: number of "converged" Ritz values.
    c          This represents the number of Ritz values that satisfy
    c          the convergence criterion.
    c
    c          IPARAM(6) = IUPD
    c          No longer referenced. Implicit restarting is ALWAYS used.  
    c
    c          IPARAM(7) = MODE
    c          On INPUT determines what type of eigenproblem is being solved.
    c          Must be 1,2,3,4; See under \Description of snaupd for the 
    c          four modes available.
    c
    c          IPARAM(8) = NP
    c          When ido = 3 and the user provides shifts through reverse
    c          communication (IPARAM(1)=0), snaupd returns NP, the number
    c          of shifts the user is to provide. 0 < NP <=NCV-NEV. See Remark
    c          5 below.
    c
    c          IPARAM(9) = NUMOP, IPARAM(10) = NUMOPB, IPARAM(11) = NUMREO,
    c          OUTPUT: NUMOP  = total number of OP*x operations,
    c                  NUMOPB = total number of B*x operations if BMAT='G',
    c                  NUMREO = total number of steps of re-orthogonalization.        
    c
    c  IPNTR   Integer array of length 14.  (OUTPUT)
    c          Pointer to mark the starting locations in the WORKD and WORKL
    c          arrays for matrices/vectors used by the Arnoldi iteration.
    c          -------------------------------------------------------------
    c          IPNTR(1): pointer to the current operand vector X in WORKD.
    c          IPNTR(2): pointer to the current result vector Y in WORKD.
    c          IPNTR(3): pointer to the vector B * X in WORKD when used in 
    c                    the shift-and-invert mode.
    c          IPNTR(4): pointer to the next available location in WORKL
    c                    that is untouched by the program.
    c          IPNTR(5): pointer to the NCV by NCV upper Hessenberg matrix
    c                    H in WORKL.
    c          IPNTR(6): pointer to the real part of the ritz value array 
    c                    RITZR in WORKL.
    c          IPNTR(7): pointer to the imaginary part of the ritz value array
    c                    RITZI in WORKL.
    c          IPNTR(8): pointer to the Ritz estimates in array WORKL associated
    c                    with the Ritz values located in RITZR and RITZI in WORKL.
    c
    c          IPNTR(14): pointer to the NP shifts in WORKL. See Remark 5 below.
    c
    c          Note: IPNTR(9:13) is only referenced by sneupd. See Remark 2 below.
    c
    c          IPNTR(9):  pointer to the real part of the NCV RITZ values of the 
    c                     original system.
    c          IPNTR(10): pointer to the imaginary part of the NCV RITZ values of 
    c                     the original system.
    c          IPNTR(11): pointer to the NCV corresponding error bounds.
    c          IPNTR(12): pointer to the NCV by NCV upper quasi-triangular
    c                     Schur matrix for H.
    c          IPNTR(13): pointer to the NCV by NCV matrix of eigenvectors
    c                     of the upper Hessenberg matrix H. Only referenced by
    c                     sneupd if RVEC = .TRUE. See Remark 2 below.
    c          -------------------------------------------------------------
    c          
    c  WORKD   Real  work array of length 3*N.  (REVERSE COMMUNICATION)
    c          Distributed array to be used in the basic Arnoldi iteration
    c          for reverse communication.  The user should not use WORKD 
    c          as temporary workspace during the iteration. Upon termination
    c          WORKD(1:N) contains B*RESID(1:N). If an invariant subspace
    c          associated with the converged Ritz values is desired, see remark
    c          2 below, subroutine sneupd uses this output.
    c          See Data Distribution Note below.  
    c
    c  WORKL   Real  work array of length LWORKL.  (OUTPUT/WORKSPACE)
    c          Private (replicated) array on each PE or array allocated on
    c          the front end.  See Data Distribution Note below.
    c
    c  LWORKL  Integer.  (INPUT)
    c          LWORKL must be at least 3*NCV**2 + 6*NCV.
    c
    c  INFO    Integer.  (INPUT/OUTPUT)
    c          If INFO .EQ. 0, a randomly initial residual vector is used.
    c          If INFO .NE. 0, RESID contains the initial residual vector,
    c                          possibly from a previous run.
    c          Error flag on output.
    c          =  0: Normal exit.
    c          =  1: Maximum number of iterations taken.
    c                All possible eigenvalues of OP has been found. IPARAM(5)  
    c                returns the number of wanted converged Ritz values.
    c          =  2: No longer an informational error. Deprecated starting
    c                with release 2 of ARPACK.
    c          =  3: No shifts could be applied during a cycle of the 
    c                Implicitly restarted Arnoldi iteration. One possibility 
    c                is to increase the size of NCV relative to NEV. 
    c                See remark 4 below.
    c          = -1: N must be positive.
    c          = -2: NEV must be positive.
    c          = -3: NCV-NEV >= 2 and less than or equal to N.
    c          = -4: The maximum number of Arnoldi update iteration 
    c                must be greater than zero.
    c          = -5: WHICH must be one of 'LM', 'SM', 'LR', 'SR', 'LI', 'SI'
    c          = -6: BMAT must be one of 'I' or 'G'.
    c          = -7: Length of private work array is not sufficient.
    c          = -8: Error return from LAPACK eigenvalue calculation;
    c          = -9: Starting vector is zero.
    c          = -10: IPARAM(7) must be 1,2,3,4.
    c          = -11: IPARAM(7) = 1 and BMAT = 'G' are incompatible.
    c          = -12: IPARAM(1) must be equal to 0 or 1.
    c          = -9999: Could not build an Arnoldi factorization.
    c                   IPARAM(5) returns the size of the current Arnoldi
    c                   factorization.
    c
    c\Remarks
    c  1. The computed Ritz values are approximate eigenvalues of OP. The
    c     selection of WHICH should be made with this in mind when
    c     Mode = 3 and 4.  After convergence, approximate eigenvalues of the
    c     original problem may be obtained with the ARPACK subroutine sneupd.
    c
    c  2. If a basis for the invariant subspace corresponding to the converged Ritz 
    c     values is needed, the user must call sneupd immediately following 
    c     completion of snaupd. This is new starting with release 2 of ARPACK.
    c
    c  3. If M can be factored into a Cholesky factorization M = LL`
    c     then Mode = 2 should not be selected.  Instead one should use
    c     Mode = 1 with  OP = inv(L)*A*inv(L`).  Appropriate triangular 
    c     linear systems should be solved with L and L` rather
    c     than computing inverses.  After convergence, an approximate
    c     eigenvector z of the original problem is recovered by solving
    c     L`z = x  where x is a Ritz vector of OP.
    c
    c  4. At present there is no a-priori analysis to guide the selection
    c     of NCV relative to NEV.  The only formal requrement is that NCV > NEV + 2.
    c     However, it is recommended that NCV .ge. 2*NEV+1.  If many problems of
    c     the same type are to be solved, one should experiment with increasing
    c     NCV while keeping NEV fixed for a given test problem.  This will 
    c     usually decrease the required number of OP*x operations but it
    c     also increases the work and storage required to maintain the orthogonal
    c     basis vectors.  The optimal "cross-over" with respect to CPU time
    c     is problem dependent and must be determined empirically. 
    c     See Chapter 8 of Reference 2 for further information.
    c
    c  5. When IPARAM(1) = 0, and IDO = 3, the user needs to provide the 
    c     NP = IPARAM(8) real and imaginary parts of the shifts in locations 
    c         real part                  imaginary part
    c         -----------------------    --------------
    c     1   WORKL(IPNTR(14))           WORKL(IPNTR(14)+NP)
    c     2   WORKL(IPNTR(14)+1)         WORKL(IPNTR(14)+NP+1)
    c                        .                          .
    c                        .                          .
    c                        .                          .
    c     NP  WORKL(IPNTR(14)+NP-1)      WORKL(IPNTR(14)+2*NP-1).
    c
    c     Only complex conjugate pairs of shifts may be applied and the pairs 
    c     must be placed in consecutive locations. The real part of the 
    c     eigenvalues of the current upper Hessenberg matrix are located in 
    c     WORKL(IPNTR(6)) through WORKL(IPNTR(6)+NCV-1) and the imaginary part 
    c     in WORKL(IPNTR(7)) through WORKL(IPNTR(7)+NCV-1). They are ordered
    c     according to the order defined by WHICH. The complex conjugate
    c     pairs are kept together and the associated Ritz estimates are located in
    c     WORKL(IPNTR(8)), WORKL(IPNTR(8)+1), ... , WORKL(IPNTR(8)+NCV-1).
  */
  
    void snaupd_(int* ido, char const* bmat, int* n, char const* which, int* nev,
                  float* tol, float* resid, int* ncv, float* v, int* ldv,
                  int* iparam, int* ipntr, float* workd, float* workl,
                  int* lworkl, int* info);

  /*
    c\Name: sneupd
    c
    c\Description: 
    c
    c  This subroutine returns the converged approximations to eigenvalues
    c  of A*z = lambda*B*z and (optionally):
    c
    c      (1) The corresponding approximate eigenvectors;
    c
    c      (2) An orthonormal basis for the associated approximate
    c          invariant subspace;
    c
    c      (3) Both.
    c
    c  There is negligible additional cost to obtain eigenvectors.  An orthonormal
    c  basis is always computed.  There is an additional storage cost of n*nev
    c  if both are requested (in this case a separate array Z must be supplied).
    c
    c  The approximate eigenvalues and eigenvectors of  A*z = lambda*B*z
    c  are derived from approximate eigenvalues and eigenvectors of
    c  of the linear operator OP prescribed by the MODE selection in the
    c  call to SNAUPD.  SNAUPD must be called before this routine is called.
    c  These approximate eigenvalues and vectors are commonly called Ritz
    c  values and Ritz vectors respectively.  They are referred to as such
    c  in the comments that follow.  The computed orthonormal basis for the
    c  invariant subspace corresponding to these Ritz values is referred to as a
    c  Schur basis.
    c
    c  See documentation in the header of the subroutine SNAUPD for 
    c  definition of OP as well as other terms and the relation of computed
    c  Ritz values and Ritz vectors of OP with respect to the given problem
    c  A*z = lambda*B*z.  For a brief description, see definitions of 
    c  IPARAM(7), MODE and WHICH in the documentation of SNAUPD.
    c
    c\Usage:
    c  call sneupd 
    c     ( RVEC, HOWMNY, SELECT, DR, DI, Z, LDZ, SIGMAR, SIGMAI, WORKEV, BMAT, 
    c       N, WHICH, NEV, TOL, RESID, NCV, V, LDV, IPARAM, IPNTR, WORKD, WORKL, 
    c       LWORKL, INFO )
    c
    c\Arguments:
    c  RVEC    LOGICAL  (INPUT) 
    c          Specifies whether a basis for the invariant subspace corresponding 
    c          to the converged Ritz value approximations for the eigenproblem 
    c          A*z = lambda*B*z is computed.
    c
    c             RVEC = .FALSE.     Compute Ritz values only.
    c
    c             RVEC = .TRUE.      Compute the Ritz vectors or Schur vectors.
    c                                See Remarks below. 
    c 
    c  HOWMNY  Character*1  (INPUT) 
    c          Specifies the form of the basis for the invariant subspace 
    c          corresponding to the converged Ritz values that is to be computed.
    c
    c          = 'A': Compute NEV Ritz vectors; 
    c          = 'P': Compute NEV Schur vectors;
    c          = 'S': compute some of the Ritz vectors, specified
    c                 by the logical array SELECT.
    c
    c  SELECT  Logical array of dimension NCV.  (INPUT)
    c          If HOWMNY = 'S', SELECT specifies the Ritz vectors to be
    c          computed. To select the Ritz vector corresponding to a
    c          Ritz value (DR(j), DI(j)), SELECT(j) must be set to .TRUE.. 
    c          If HOWMNY = 'A' or 'P', SELECT is used as internal workspace.
    c
    c  DR      Real  array of dimension NEV+1.  (OUTPUT)
    c          If IPARAM(7) = 1,2 or 3 and SIGMAI=0.0  then on exit: DR contains 
    c          the real part of the Ritz  approximations to the eigenvalues of 
    c          A*z = lambda*B*z. 
    c          If IPARAM(7) = 3, 4 and SIGMAI is not equal to zero, then on exit:
    c          DR contains the real part of the Ritz values of OP computed by 
    c          SNAUPD. A further computation must be performed by the user
    c          to transform the Ritz values computed for OP by SNAUPD to those
    c          of the original system A*z = lambda*B*z. See remark 3 below.
    c
    c  DI      Real  array of dimension NEV+1.  (OUTPUT)
    c          On exit, DI contains the imaginary part of the Ritz value 
    c          approximations to the eigenvalues of A*z = lambda*B*z associated
    c          with DR.
    c
    c          NOTE: When Ritz values are complex, they will come in complex 
    c                conjugate pairs.  If eigenvectors are requested, the 
    c                corresponding Ritz vectors will also come in conjugate 
    c                pairs and the real and imaginary parts of these are 
    c                represented in two consecutive columns of the array Z 
    c                (see below).
    c
    c  Z       Real  N by NEV+1 array if RVEC = .TRUE. and HOWMNY = 'A'. (OUTPUT)
    c          On exit, if RVEC = .TRUE. and HOWMNY = 'A', then the columns of 
    c          Z represent approximate eigenvectors (Ritz vectors) corresponding 
    c          to the NCONV=IPARAM(5) Ritz values for eigensystem 
    c          A*z = lambda*B*z. 
    c 
    c          The complex Ritz vector associated with the Ritz value 
    c          with positive imaginary part is stored in two consecutive 
    c          columns.  The first column holds the real part of the Ritz 
    c          vector and the second column holds the imaginary part.  The 
    c          Ritz vector associated with the Ritz value with negative 
    c          imaginary part is simply the complex conjugate of the Ritz vector 
    c          associated with the positive imaginary part.
    c
    c          If  RVEC = .FALSE. or HOWMNY = 'P', then Z is not referenced.
    c
    c          NOTE: If if RVEC = .TRUE. and a Schur basis is not required,
    c          the array Z may be set equal to first NEV+1 columns of the Arnoldi
    c          basis array V computed by SNAUPD.  In this case the Arnoldi basis
    c          will be destroyed and overwritten with the eigenvector basis.
    c
    c  LDZ     Integer.  (INPUT)
    c          The leading dimension of the array Z.  If Ritz vectors are
    c          desired, then  LDZ >= max( 1, N ).  In any case,  LDZ >= 1.
    c
    c  SIGMAR  Real   (INPUT)
    c          If IPARAM(7) = 3 or 4, represents the real part of the shift. 
    c          Not referenced if IPARAM(7) = 1 or 2.
    c
    c  SIGMAI  Real   (INPUT)
    c          If IPARAM(7) = 3 or 4, represents the imaginary part of the shift. 
    c          Not referenced if IPARAM(7) = 1 or 2. See remark 3 below.
    c
    c  WORKEV  Real  work array of dimension 3*NCV.  (WORKSPACE)
    c
    c  **** The remaining arguments MUST be the same as for the   ****
    c  **** call to SNAUPD that was just completed.               ****
    c
    c  NOTE: The remaining arguments
    c
    c           BMAT, N, WHICH, NEV, TOL, RESID, NCV, V, LDV, IPARAM, IPNTR,
    c           WORKD, WORKL, LWORKL, INFO
    c
    c         must be passed directly to SNEUPD following the last call
    c         to SNAUPD.  These arguments MUST NOT BE MODIFIED between
    c         the the last call to SNAUPD and the call to SNEUPD.
    c
    c  Three of these parameters (V, WORKL, INFO) are also output parameters:
    c
    c  V       Real  N by NCV array.  (INPUT/OUTPUT)
    c
    c          Upon INPUT: the NCV columns of V contain the Arnoldi basis
    c                      vectors for OP as constructed by SNAUPD .
    c
    c          Upon OUTPUT: If RVEC = .TRUE. the first NCONV=IPARAM(5) columns
    c                       contain approximate Schur vectors that span the
    c                       desired invariant subspace.  See Remark 2 below.
    c
    c          NOTE: If the array Z has been set equal to first NEV+1 columns
    c          of the array V and RVEC=.TRUE. and HOWMNY= 'A', then the
    c          Arnoldi basis held by V has been overwritten by the desired
    c          Ritz vectors.  If a separate array Z has been passed then
    c          the first NCONV=IPARAM(5) columns of V will contain approximate
    c          Schur vectors that span the desired invariant subspace.
    c
    c  WORKL   Real  work array of length LWORKL.  (OUTPUT/WORKSPACE)
    c          WORKL(1:ncv*ncv+3*ncv) contains information obtained in
    c          snaupd.  They are not changed by sneupd.
    c          WORKL(ncv*ncv+3*ncv+1:3*ncv*ncv+6*ncv) holds the
    c          real and imaginary part of the untransformed Ritz values,
    c          the upper quasi-triangular matrix for H, and the
    c          associated matrix representation of the invariant subspace for H.
    c
    c          Note: IPNTR(9:13) contains the pointer into WORKL for addresses
    c          of the above information computed by sneupd.
    c          -------------------------------------------------------------
    c          IPNTR(9):  pointer to the real part of the NCV RITZ values of the
    c                     original system.
    c          IPNTR(10): pointer to the imaginary part of the NCV RITZ values of
    c                     the original system.
    c          IPNTR(11): pointer to the NCV corresponding error bounds.
    c          IPNTR(12): pointer to the NCV by NCV upper quasi-triangular
    c                     Schur matrix for H.
    c          IPNTR(13): pointer to the NCV by NCV matrix of eigenvectors
    c                     of the upper Hessenberg matrix H. Only referenced by
    c                     sneupd if RVEC = .TRUE. See Remark 2 below.
    c          -------------------------------------------------------------
    c
    c  INFO    Integer.  (OUTPUT)
    c          Error flag on output.
    c
    c          =  0: Normal exit.
    c
    c          =  1: The Schur form computed by LAPACK routine slahqr
    c                could not be reordered by LAPACK routine strsen.
    c                Re-enter subroutine sneupd with IPARAM(5)=NCV and 
    c                increase the size of the arrays DR and DI to have 
    c                dimension at least dimension NCV and allocate at least NCV 
    c                columns for Z. NOTE: Not necessary if Z and V share 
    c                the same space. Please notify the authors if this error
    c                occurs.
    c
    c          = -1: N must be positive.
    c          = -2: NEV must be positive.
    c          = -3: NCV-NEV >= 2 and less than or equal to N.
    c          = -5: WHICH must be one of 'LM', 'SM', 'LR', 'SR', 'LI', 'SI'
    c          = -6: BMAT must be one of 'I' or 'G'.
    c          = -7: Length of private work WORKL array is not sufficient.
    c          = -8: Error return from calculation of a real Schur form.
    c                Informational error from LAPACK routine slahqr.
    c          = -9: Error return from calculation of eigenvectors.
    c                Informational error from LAPACK routine strevc.
    c          = -10: IPARAM(7) must be 1,2,3,4.
    c          = -11: IPARAM(7) = 1 and BMAT = 'G' are incompatible.
    c          = -12: HOWMNY = 'S' not yet implemented
    c          = -13: HOWMNY must be one of 'A' or 'P' if RVEC = .true.
    c          = -14: SNAUPD did not find any eigenvalues to sufficient
    c                 accuracy.
    c          = -15: DNEUPD got a different count of the number of converged
    c                 Ritz values than DNAUPD got.  This indicates the user
    c                 probably made an error in passing data from DNAUPD to
    c                 DNEUPD or that the data was modified before entering
    c                 DNEUPD
    c
  */
  
  void sneupd_(bool* rvec, char const* howmny, int const* select, float* dr,
                float* di, float* z, int* ldz, float* sigmar, float* sigmai, char
                const* bmat, int* n, char const* which, int* nev, float* tol,
                float* resid, int* ncv, float* v, int* ldv, int* iparam,
                int* ipntr, float* workd, float* workl, int* lworkl, int* info);
  
  void ssaupd_(int* ido, char const* bmat, int* n, char const* which, int* nev,
                float* tol, float* resid, int* ncv, float* v, int* ldv,
                int* iparam, int* ipntr, float* workd, float* workl, int* lworkl,
                int* info);
  
  void sseupd_(bool* rvec, char const* howmny, int const* select, float* d,
                float* z, int* ldz, float* sigma, char const* bmat, int* n,
                char const* which, int* nev, float* tol, float* resid, int* ncv,
                float* v, int* ldv, int* iparam, int* ipntr, float* workd,
                float* workl, int* lworkl, int* info);

  /*
    c\Name: znaupd
    c
    c\Description:
    c  Reverse communication interface for the Implicitly Restarted Arnoldi
    c  iteration. This is intended to be used to find a few eigenpairs of a
    c  complex linear operator OP with respect to a semi-inner product defined
    c  by a hermitian positive semi-definite real matrix B. B may be the identity
    c  matrix.  NOTE: if both OP and B are real, then dsaupd  or dnaupd  should
    c  be used.
    c
    c
    c  The computed approximate eigenvalues are called Ritz values and
    c  the corresponding approximate eigenvectors are called Ritz vectors.
    c
    c  znaupd  is usually called iteratively to solve one of the
    c  following problems:
    c
    c  Mode 1:  A*x = lambda*x.
    c           ===> OP = A  and  B = I.
    c
    c  Mode 2:  A*x = lambda*M*x, M hermitian positive definite
    c           ===> OP = inv[M]*A  and  B = M.
    c           ===> (If M can be factored see remark 3 below)
    c
    c  Mode 3:  A*x = lambda*M*x, M hermitian semi-definite
    c           ===> OP =  inv[A - sigma*M]*M   and  B = M.
    c           ===> shift-and-invert mode
    c           If OP*x = amu*x, then lambda = sigma + 1/amu.
    c
    c
    c  NOTE: The action of w <- inv[A - sigma*M]*v or w <- inv[M]*v
    c        should be accomplished either by a direct method
    c        using a sparse matrix factorization and solving
    c
    c           [A - sigma*M]*w = v  or M*w = v,
    c
    c        or through an iterative method for solving these
    c        systems.  If an iterative method is used, the
    c        convergence test must be more stringent than
    c        the accuracy requirements for the eigenvalue
    c        approximations.
    c
    c\Usage:
    c  call znaupd
    c     ( IDO, BMAT, N, WHICH, NEV, TOL, RESID, NCV, V, LDV, IPARAM,
    c       IPNTR, WORKD, WORKL, LWORKL, RWORK, INFO )
    c
    c\Arguments
    c  IDO     Integer.  (INPUT/OUTPUT)
    c          Reverse communication flag.  IDO must be zero on the first
    c          call to znaupd .  IDO will be set internally to
    c          indicate the type of operation to be performed.  Control is
    c          then given back to the calling routine which has the
    c          responsibility to carry out the requested operation and call
    c          znaupd  with the result.  The operand is given in
    c          WORKD(IPNTR(1)), the result must be put in WORKD(IPNTR(2)).
    c          -------------------------------------------------------------
    c          IDO =  0: first call to the reverse communication interface
    c          IDO = -1: compute  Y = OP * X  where
    c                    IPNTR(1) is the pointer into WORKD for X,
    c                    IPNTR(2) is the pointer into WORKD for Y.
    c                    This is for the initialization phase to force the
    c                    starting vector into the range of OP.
    c          IDO =  1: compute  Y = OP * X  where
    c                    IPNTR(1) is the pointer into WORKD for X,
    c                    IPNTR(2) is the pointer into WORKD for Y.
    c                    In mode 3, the vector B * X is already
    c                    available in WORKD(ipntr(3)).  It does not
    c                    need to be recomputed in forming OP * X.
    c          IDO =  2: compute  Y = M * X  where
    c                    IPNTR(1) is the pointer into WORKD for X,
    c                    IPNTR(2) is the pointer into WORKD for Y.
    c          IDO =  3: compute and return the shifts in the first
    c                    NP locations of WORKL.
    c          IDO = 99: done
    c          -------------------------------------------------------------
    c          After the initialization phase, when the routine is used in
    c          the "shift-and-invert" mode, the vector M * X is already
    c          available and does not need to be recomputed in forming OP*X.
    c
    c  BMAT    Character*1.  (INPUT)
    c          BMAT specifies the type of the matrix B that defines the
    c          semi-inner product for the operator OP.
    c          BMAT = 'I' -> standard eigenvalue problem A*x = lambda*x
    c          BMAT = 'G' -> generalized eigenvalue problem A*x = lambda*M*x
    c
    c  N       Integer.  (INPUT)
    c          Dimension of the eigenproblem.
    c
    c  WHICH   Character*2.  (INPUT)
    c          'LM' -> want the NEV eigenvalues of largest magnitude.
    c          'SM' -> want the NEV eigenvalues of smallest magnitude.
    c          'LR' -> want the NEV eigenvalues of largest real part.
    c          'SR' -> want the NEV eigenvalues of smallest real part.
    c          'LI' -> want the NEV eigenvalues of largest imaginary part.
    c          'SI' -> want the NEV eigenvalues of smallest imaginary part.
    c
    c  NEV     Integer.  (INPUT)
    c          Number of eigenvalues of OP to be computed. 0 < NEV < N-1.
    c
    c  TOL     Double precision   scalar.  (INPUT)
    c          Stopping criteria: the relative accuracy of the Ritz value
    c          is considered acceptable if BOUNDS(I) .LE. TOL*ABS(RITZ(I))
    c          where ABS(RITZ(I)) is the magnitude when RITZ(I) is complex.
    c          DEFAULT = dlamch ('EPS')  (machine precision as computed
    c                    by the LAPACK auxiliary subroutine dlamch ).
    c
    c  RESID   Complex*16  array of length N.  (INPUT/OUTPUT)
    c          On INPUT:
    c          If INFO .EQ. 0, a random initial residual vector is used.
    c          If INFO .NE. 0, RESID contains the initial residual vector,
    c                          possibly from a previous run.
    c          On OUTPUT:
    c          RESID contains the final residual vector.
    c
    c  NCV     Integer.  (INPUT)
    c          Number of columns of the matrix V. NCV must satisfy the two
    c          inequalities 1 <= NCV-NEV and NCV <= N.
    c          This will indicate how many Arnoldi vectors are generated
    c          at each iteration.  After the startup phase in which NEV
    c          Arnoldi vectors are generated, the algorithm generates
    c          approximately NCV-NEV Arnoldi vectors at each subsequent update
    c          iteration. Most of the cost in generating each Arnoldi vector is
    c          in the matrix-vector operation OP*x. (See remark 4 below.)
    c
    c  V       Complex*16  array N by NCV.  (OUTPUT)
    c          Contains the final set of Arnoldi basis vectors.
    c
    c  LDV     Integer.  (INPUT)
    c          Leading dimension of V exactly as declared in the calling program.
    c
    c  IPARAM  Integer array of length 11.  (INPUT/OUTPUT)
    c          IPARAM(1) = ISHIFT: method for selecting the implicit shifts.
    c          The shifts selected at each iteration are used to filter out
    c          the components of the unwanted eigenvector.
    c          -------------------------------------------------------------
    c          ISHIFT = 0: the shifts are to be provided by the user via
    c                      reverse communication.  The NCV eigenvalues of
    c                      the Hessenberg matrix H are returned in the part
    c                      of WORKL array corresponding to RITZ.
    c          ISHIFT = 1: exact shifts with respect to the current
    c                      Hessenberg matrix H.  This is equivalent to
    c                      restarting the iteration from the beginning
    c                      after updating the starting vector with a linear
    c                      combination of Ritz vectors associated with the
    c                      "wanted" eigenvalues.
    c          ISHIFT = 2: other choice of internal shift to be defined.
    c          -------------------------------------------------------------
    c
    c          IPARAM(2) = No longer referenced
    c
    c          IPARAM(3) = MXITER
    c          On INPUT:  maximum number of Arnoldi update iterations allowed.
    c          On OUTPUT: actual number of Arnoldi update iterations taken.
    c
    c          IPARAM(4) = NB: blocksize to be used in the recurrence.
    c          The code currently works only for NB = 1.
    c
    c          IPARAM(5) = NCONV: number of "converged" Ritz values.
    c          This represents the number of Ritz values that satisfy
    c          the convergence criterion.
    c
    c          IPARAM(6) = IUPD
    c          No longer referenced. Implicit restarting is ALWAYS used.
    c
    c          IPARAM(7) = MODE
    c          On INPUT determines what type of eigenproblem is being solved.
    c          Must be 1,2,3; See under \Description of znaupd  for the
    c          four modes available.
    c
    c          IPARAM(8) = NP
    c          When ido = 3 and the user provides shifts through reverse
    c          communication (IPARAM(1)=0), _naupd returns NP, the number
    c          of shifts the user is to provide. 0 < NP < NCV-NEV.
    c
    c          IPARAM(9) = NUMOP, IPARAM(10) = NUMOPB, IPARAM(11) = NUMREO,
    c          OUTPUT: NUMOP  = total number of OP*x operations,
    c                  NUMOPB = total number of B*x operations if BMAT='G',
    c                  NUMREO = total number of steps of re-orthogonalization.
    c
    c  IPNTR   Integer array of length 14.  (OUTPUT)
    c          Pointer to mark the starting locations in the WORKD and WORKL
    c          arrays for matrices/vectors used by the Arnoldi iteration.
    c          -------------------------------------------------------------
    c          IPNTR(1): pointer to the current operand vector X in WORKD.
    c          IPNTR(2): pointer to the current result vector Y in WORKD.
    c          IPNTR(3): pointer to the vector B * X in WORKD when used in
    c                    the shift-and-invert mode.
    c          IPNTR(4): pointer to the next available location in WORKL
    c                    that is untouched by the program.
    c          IPNTR(5): pointer to the NCV by NCV upper Hessenberg
    c                    matrix H in WORKL.
    c          IPNTR(6): pointer to the  ritz value array  RITZ
    c          IPNTR(7): pointer to the (projected) ritz vector array Q
    c          IPNTR(8): pointer to the error BOUNDS array in WORKL.
    c          IPNTR(14): pointer to the NP shifts in WORKL. See Remark 5 below.
    c
    c          Note: IPNTR(9:13) is only referenced by zneupd . See Remark 2 below.
    c
    c          IPNTR(9): pointer to the NCV RITZ values of the
    c                    original system.
    c          IPNTR(10): Not Used
    c          IPNTR(11): pointer to the NCV corresponding error bounds.
    c          IPNTR(12): pointer to the NCV by NCV upper triangular
    c                     Schur matrix for H.
    c          IPNTR(13): pointer to the NCV by NCV matrix of eigenvectors
    c                     of the upper Hessenberg matrix H. Only referenced by
    c                     zneupd  if RVEC = .TRUE. See Remark 2 below.
    c
    c          -------------------------------------------------------------
    c
    c  WORKD   Complex*16  work array of length 3*N.  (REVERSE COMMUNICATION)
    c          Distributed array to be used in the basic Arnoldi iteration
    c          for reverse communication.  The user should not use WORKD
    c          as temporary workspace during the iteration !!!!!!!!!!
    c          See Data Distribution Note below.
    c
    c  WORKL   Complex*16  work array of length LWORKL.  (OUTPUT/WORKSPACE)
    c          Private (replicated) array on each PE or array allocated on
    c          the front end.  See Data Distribution Note below.
    c
    c  LWORKL  Integer.  (INPUT)
    c          LWORKL must be at least 3*NCV**2 + 5*NCV.
    c
    c  RWORK   Double precision   work array of length NCV (WORKSPACE)
    c          Private (replicated) array on each PE or array allocated on
    c          the front end.
    c
    c
    c  INFO    Integer.  (INPUT/OUTPUT)
    c          If INFO .EQ. 0, a randomly initial residual vector is used.
    c          If INFO .NE. 0, RESID contains the initial residual vector,
    c                          possibly from a previous run.
    c          Error flag on output.
    c          =  0: Normal exit.
    c          =  1: Maximum number of iterations taken.
    c                All possible eigenvalues of OP has been found. IPARAM(5)
    c                returns the number of wanted converged Ritz values.
    c          =  2: No longer an informational error. Deprecated starting
    c                with release 2 of ARPACK.
    c          =  3: No shifts could be applied during a cycle of the
    c                Implicitly restarted Arnoldi iteration. One possibility
    c                is to increase the size of NCV relative to NEV.
    c                See remark 4 below.
    c          = -1: N must be positive.
    c          = -2: NEV must be positive.
    c          = -3: NCV-NEV >= 2 and less than or equal to N.
    c          = -4: The maximum number of Arnoldi update iteration
    c                must be greater than zero.
    c          = -5: WHICH must be one of 'LM', 'SM', 'LR', 'SR', 'LI', 'SI'
    c          = -6: BMAT must be one of 'I' or 'G'.
    c          = -7: Length of private work array is not sufficient.
    c          = -8: Error return from LAPACK eigenvalue calculation;
    c          = -9: Starting vector is zero.
    c          = -10: IPARAM(7) must be 1,2,3.
    c          = -11: IPARAM(7) = 1 and BMAT = 'G' are incompatible.
    c          = -12: IPARAM(1) must be equal to 0 or 1.
    c          = -9999: Could not build an Arnoldi factorization.
    c                   User input error highly likely.  Please
    c                   check actual array dimensions and layout.
    c                   IPARAM(5) returns the size of the current Arnoldi
    c                   factorization.
    c
    c\Remarks
    c  1. The computed Ritz values are approximate eigenvalues of OP. The
    c     selection of WHICH should be made with this in mind when using
    c     Mode = 3.  When operating in Mode = 3 setting WHICH = 'LM' will
    c     compute the NEV eigenvalues of the original problem that are
    c     closest to the shift SIGMA . After convergence, approximate eigenvalues
    c     of the original problem may be obtained with the ARPACK subroutine zneupd .
    c
    c  2. If a basis for the invariant subspace corresponding to the converged Ritz
    c     values is needed, the user must call zneupd  immediately following
    c     completion of znaupd . This is new starting with release 2 of ARPACK.
    c
    c  3. If M can be factored into a Cholesky factorization M = LL`
    c     then Mode = 2 should not be selected.  Instead one should use
    c     Mode = 1 with  OP = inv(L)*A*inv(L`).  Appropriate triangular
    c     linear systems should be solved with L and L` rather
    c     than computing inverses.  After convergence, an approximate
    c     eigenvector z of the original problem is recovered by solving
    c     L`z = x  where x is a Ritz vector of OP.
    c
    c  4. At present there is no a-priori analysis to guide the selection
    c     of NCV relative to NEV.  The only formal requirement is that NCV > NEV + 1.
    c     However, it is recommended that NCV .ge. 2*NEV.  If many problems of
    c     the same type are to be solved, one should experiment with increasing
    c     NCV while keeping NEV fixed for a given test problem.  This will
    c     usually decrease the required number of OP*x operations but it
    c     also increases the work and storage required to maintain the orthogonal
    c     basis vectors.  The optimal "cross-over" with respect to CPU time
    c     is problem dependent and must be determined empirically.
    c     See Chapter 8 of Reference 2 for further information.
    c
    c  5. When IPARAM(1) = 0, and IDO = 3, the user needs to provide the
    c     NP = IPARAM(8) complex shifts in locations
    c     WORKL(IPNTR(14)), WORKL(IPNTR(14)+1), ... , WORKL(IPNTR(14)+NP).
    c     Eigenvalues of the current upper Hessenberg matrix are located in
    c     WORKL(IPNTR(6)) through WORKL(IPNTR(6)+NCV-1). They are ordered
    c     according to the order defined by WHICH.  The associated Ritz estimates
    c     are located in WORKL(IPNTR(8)), WORKL(IPNTR(8)+1), ... ,
    c     WORKL(IPNTR(8)+NCV-1).
    
  */
    
  void znaupd_(int* ido, char const* bmat, int* n, char const* which, int* nev,
               double * tol, std::complex<double>* resid, int* ncv, std::complex<double>* v,
               int* ldv, int* iparam, int* ipntr, std::complex<double>* workd,
               std::complex<double>* workl, int* lworkl, double* rwork, int* info);

  /*
    c\Name: zneupd 
    c 
    c\Description: 
    c  This subroutine returns the converged approximations to eigenvalues 
    c  of A*z = lambda*B*z and (optionally): 
    c 
    c      (1) The corresponding approximate eigenvectors; 
    c 
    c      (2) An orthonormal basis for the associated approximate 
    c          invariant subspace; 
    c 
    c      (3) Both.  
    c
    c  There is negligible additional cost to obtain eigenvectors.  An orthonormal 
    c  basis is always computed.  There is an additional storage cost of n*nev
    c  if both are requested (in this case a separate array Z must be supplied). 
    c
    c  The approximate eigenvalues and eigenvectors of  A*z = lambda*B*z
    c  are derived from approximate eigenvalues and eigenvectors of
    c  of the linear operator OP prescribed by the MODE selection in the
    c  call to ZNAUPD.  ZNAUPD must be called before this routine is called.
    c  These approximate eigenvalues and vectors are commonly called Ritz
    c  values and Ritz vectors respectively.  They are referred to as such 
    c  in the comments that follow.   The computed orthonormal basis for the 
    c  invariant subspace corresponding to these Ritz values is referred to as a 
    c  Schur basis. 
    c 
    c  The definition of OP as well as other terms and the relation of computed
    c  Ritz values and vectors of OP with respect to the given problem
    c  A*z = lambda*B*z may be found in the header of ZNAUPD.  For a brief 
    c  description, see definitions of IPARAM(7), MODE and WHICH in the
    c  documentation of ZNAUPD.
    c
    c\Usage:
    c  call zneupd 
    c     ( RVEC, HOWMNY, SELECT, D, Z, LDZ, SIGMA, WORKEV, BMAT, 
    c       N, WHICH, NEV, TOL, RESID, NCV, V, LDV, IPARAM, IPNTR, WORKD, 
    c       WORKL, LWORKL, RWORK, INFO )
    c
    c\Arguments:
    c  RVEC    LOGICAL  (INPUT)
    c          Specifies whether a basis for the invariant subspace corresponding
    c          to the converged Ritz value approximations for the eigenproblem 
    c          A*z = lambda*B*z is computed.
    c
    c             RVEC = .FALSE.     Compute Ritz values only.
    c
    c             RVEC = .TRUE.      Compute Ritz vectors or Schur vectors.
    c                                See Remarks below.
    c
    c  HOWMNY  Character*1  (INPUT)
    c          Specifies the form of the basis for the invariant subspace 
    c          corresponding to the converged Ritz values that is to be computed.
    c
    c          = 'A': Compute NEV Ritz vectors;
    c          = 'P': Compute NEV Schur vectors;
    c          = 'S': compute some of the Ritz vectors, specified
    c                 by the logical array SELECT.
    c
    c  SELECT  Logical array of dimension NCV.  (INPUT)
    c          If HOWMNY = 'S', SELECT specifies the Ritz vectors to be
    c          computed. To select the  Ritz vector corresponding to a
    c          Ritz value D(j), SELECT(j) must be set to .TRUE.. 
    c          If HOWMNY = 'A' or 'P', SELECT need not be initialized 
    c          but it is used as internal workspace.
    c
    c  D       Complex*16 array of dimension NEV+1.  (OUTPUT)
    c          On exit, D contains the  Ritz  approximations 
    c          to the eigenvalues lambda for A*z = lambda*B*z.
    c
    c  Z       Complex*16 N by NEV array    (OUTPUT)
    c          On exit, if RVEC = .TRUE. and HOWMNY = 'A', then the columns of 
    c          Z represents approximate eigenvectors (Ritz vectors) corresponding 
    c          to the NCONV=IPARAM(5) Ritz values for eigensystem
    c          A*z = lambda*B*z.
    c
    c          If RVEC = .FALSE. or HOWMNY = 'P', then Z is NOT REFERENCED.
    c
    c          NOTE: If if RVEC = .TRUE. and a Schur basis is not required, 
    c          the array Z may be set equal to first NEV+1 columns of the Arnoldi 
    c          basis array V computed by ZNAUPD.  In this case the Arnoldi basis 
    c          will be destroyed and overwritten with the eigenvector basis.
    c
    c  LDZ     Integer.  (INPUT)
    c          The leading dimension of the array Z.  If Ritz vectors are
    c          desired, then  LDZ .ge.  max( 1, N ) is required.  
    c          In any case,  LDZ .ge. 1 is required.
    c
    c  SIGMA   Complex*16  (INPUT)
    c          If IPARAM(7) = 3 then SIGMA represents the shift. 
    c          Not referenced if IPARAM(7) = 1 or 2.
    c
    c  WORKEV  Complex*16 work array of dimension 2*NCV.  (WORKSPACE)
    c
    c  **** The remaining arguments MUST be the same as for the   ****
    c  **** call to ZNAUPD that was just completed.               ****
    c
    c  NOTE: The remaining arguments 
    c
    c           BMAT, N, WHICH, NEV, TOL, RESID, NCV, V, LDV, IPARAM, IPNTR, 
    c           WORKD, WORKL, LWORKL, RWORK, INFO 
    c
    c         must be passed directly to ZNEUPD following the last call 
    c         to ZNAUPD.  These arguments MUST NOT BE MODIFIED between
    c         the the last call to ZNAUPD and the call to ZNEUPD.
    c
    c  Three of these parameters (V, WORKL and INFO) are also output parameters:
    c
    c  V       Complex*16 N by NCV array.  (INPUT/OUTPUT)
    c
    c          Upon INPUT: the NCV columns of V contain the Arnoldi basis
    c                      vectors for OP as constructed by ZNAUPD .
    c
    c          Upon OUTPUT: If RVEC = .TRUE. the first NCONV=IPARAM(5) columns
    c                       contain approximate Schur vectors that span the
    c                       desired invariant subspace.
    c
    c          NOTE: If the array Z has been set equal to first NEV+1 columns
    c          of the array V and RVEC=.TRUE. and HOWMNY= 'A', then the
    c          Arnoldi basis held by V has been overwritten by the desired
    c          Ritz vectors.  If a separate array Z has been passed then
    c          the first NCONV=IPARAM(5) columns of V will contain approximate
    c          Schur vectors that span the desired invariant subspace.
    c
    c  WORKL   Double precision work array of length LWORKL.  (OUTPUT/WORKSPACE)
    c          WORKL(1:ncv*ncv+2*ncv) contains information obtained in
    c          znaupd.  They are not changed by zneupd.
    c          WORKL(ncv*ncv+2*ncv+1:3*ncv*ncv+4*ncv) holds the
    c          untransformed Ritz values, the untransformed error estimates of 
    c          the Ritz values, the upper triangular matrix for H, and the
    c          associated matrix representation of the invariant subspace for H.
    c
    c          Note: IPNTR(9:13) contains the pointer into WORKL for addresses
    c          of the above information computed by zneupd.
    c          -------------------------------------------------------------
    c          IPNTR(9):  pointer to the NCV RITZ values of the
    c                     original system.
    c          IPNTR(10): Not used
    c          IPNTR(11): pointer to the NCV corresponding error estimates.
    c          IPNTR(12): pointer to the NCV by NCV upper triangular
    c                     Schur matrix for H.
    c          IPNTR(13): pointer to the NCV by NCV matrix of eigenvectors
    c                     of the upper Hessenberg matrix H. Only referenced by
    c                     zneupd if RVEC = .TRUE. See Remark 2 below.
    c          -------------------------------------------------------------
    c
    c  INFO    Integer.  (OUTPUT)
    c          Error flag on output.
    c          =  0: Normal exit.
    c
    c          =  1: The Schur form computed by LAPACK routine csheqr
    c                could not be reordered by LAPACK routine ztrsen.
    c                Re-enter subroutine zneupd with IPARAM(5)=NCV and
    c                increase the size of the array D to have
    c                dimension at least dimension NCV and allocate at least NCV
    c                columns for Z. NOTE: Not necessary if Z and V share
    c                the same space. Please notify the authors if this error
    c                occurs.
    c
    c          = -1: N must be positive.
    c          = -2: NEV must be positive.
    c          = -3: NCV-NEV >= 1 and less than or equal to N.
    c          = -5: WHICH must be one of 'LM', 'SM', 'LR', 'SR', 'LI', 'SI'
    c          = -6: BMAT must be one of 'I' or 'G'.
    c          = -7: Length of private work WORKL array is not sufficient.
    c          = -8: Error return from LAPACK eigenvalue calculation.
    c                This should never happened.
    c          = -9: Error return from calculation of eigenvectors.
    c                Informational error from LAPACK routine ztrevc.
    c          = -10: IPARAM(7) must be 1,2,3
    c          = -11: IPARAM(7) = 1 and BMAT = 'G' are incompatible.
    c          = -12: HOWMNY = 'S' not yet implemented
    c          = -13: HOWMNY must be one of 'A' or 'P' if RVEC = .true.
    c          = -14: ZNAUPD did not find any eigenvalues to sufficient
    c                 accuracy.
    c          = -15: ZNEUPD got a different count of the number of converged
    c                 Ritz values than ZNAUPD got.  This indicates the user
    c                 probably made an error in passing data from ZNAUPD to
    c                 ZNEUPD or that the data was modified before entering
    c                 ZNEUPD

  */
  
  void zneupd_(bool* rvec, char const* howmny, int const* select,
                std::complex<double>* d, std::complex<double>* z, int* ldz,
                std::complex<double> * sigma, std::complex<double>* workev,
                char const* bmat, int* n, char const* which, int* nev, double* tol,
                std::complex<double>* resid, int* ncv, std::complex<double>* v, int* ldv,
                int* iparam, int* ipntr, std::complex<double>* workd,
                std::complex<double>* workl, int* lworkl, double* rwork, int* info);

}
}  // namespace detail

enum class which {
  /// 'LA' - compute the NEV largest (algebraic) eigenvalues
  largest_algebraic,
  /// 'SA' - compute the NEV smallest (algebraic) eigenvalues.
  smallest_algebraic,
  /// 'LM' - compute the NEV largest (in magnitude) eigenvalues.
  largest_magnitude,
  /// 'SM' - compute the NEV smallest (in magnitude) eigenvalues.
  smallest_magnitude,
  /// 'BE' - compute NEV eigenvalues, half from each end of the
  /// spectrum.  When NEV is odd, compute one more from the
  /// high end than from the low end.
  both_ends
};

enum class bmat {
  /// B = 'I' -> standard eigenvalue problem A*x = lambda*x
  identity,
  /// B = 'G' -> generalized eigenvalue problem A*x = lambda*B*x
  generalized
};

enum class howmny {
  /// 'A' Compute NEV Ritz vectors
  ritz_vectors,
  /// 'P' Compute NEV Schur vectors;
  schur_vectors,
  /// 'S' compute some of the Ritz vectors, specified by the logical array
  /// SELECT.
  ritz_specified
};

namespace detail {

inline char const* convert_to_char(which const option) {
  switch (option) {
    case which::largest_algebraic: {
      return "LA";
      break;
    }
    case which::smallest_algebraic: {
      return "SA";
      break;
    }
    case which::largest_magnitude: {
      return "LM";
      break;
    }
    case which::smallest_magnitude: {
      return "SM";
      break;
    }
    case which::both_ends: {
      return "BE";
      break;
    }
  }
  return "LM";
}

inline char const* convert_to_char(bmat const option) {
  return option == bmat::identity ? "I" : "G";
}

inline char const* convert_to_char(howmny const option) {
  switch (option) {
    case howmny::ritz_vectors: {
      return "A";
      break;
    }
    case howmny::schur_vectors: {
      return "P";
      break;
    }
    case howmny::ritz_specified: {
      return "S";
      break;
    }
  }
  return "A";
}

}


inline void saupd(int& ido, bmat const bmat_option, int n,
                  which const ritz_option, int nev, float tol, float* resid,
                  int ncv, float* v, int ldv, int* iparam, int* ipntr,
                  float* workd, float* workl, int lworkl, int& info) {
  detail::ssaupd_(&ido, detail::convert_to_char(bmat_option), &n,
                  detail::convert_to_char(ritz_option), &nev, &tol, resid,
                  &ncv, v, &ldv, iparam, ipntr, workd, workl, &lworkl, &info);
}

inline void seupd(bool rvec, howmny const howmny_option, int* select, float* d,
                  float* z, int ldz, float sigma, bmat const bmat_option, int n,
                  which const ritz_option, int nev, float tol, float* resid,
                  int ncv, float* v, int ldv, int* iparam, int* ipntr,
                  float* workd, float* workl, int lworkl, int& info) {
  detail::sseupd_(&rvec, detail::convert_to_char(howmny_option), select, d,
                  z, &ldz, &sigma, detail::convert_to_char(bmat_option), &n,
                  detail::convert_to_char(ritz_option), &nev, &tol, resid,
                  &ncv, v, &ldv, iparam, ipntr, workd, workl, &lworkl, &info);
}

inline void saupd(int& ido, bmat const bmat_option, int n,
                  which const ritz_option, int nev, double tol, double* resid,
                  int ncv, double* v, int ldv, int* iparam, int* ipntr,
                  double* workd, double* workl, int lworkl, int& info) {
  detail::dsaupd_(&ido, detail::convert_to_char(bmat_option), &n,
                  detail::convert_to_char(ritz_option), &nev, &tol, resid,
                  &ncv, v, &ldv, iparam, ipntr, workd, workl, &lworkl, &info);
}

inline void seupd(bool rvec, howmny const howmny_option, int* select, double* d,
                  double* z, int ldz, double sigma, bmat const bmat_option,
                  int n, which const ritz_option, int nev, double tol,
                  double* resid, int ncv, double* v, int ldv, int* iparam,
                  int* ipntr, double* workd, double* workl, int lworkl,
                  int& info) {
  detail::dseupd_(&rvec, detail::convert_to_char(howmny_option), select, d,
                   z, &ldz, &sigma, detail::convert_to_char(bmat_option), &n,
                   detail::convert_to_char(ritz_option), &nev, &tol, resid,
                   &ncv, v, &ldv, iparam, ipntr, workd, workl, &lworkl, &info);
}

inline void naupd(int& ido, bmat const bmat_option, int n,
                  which const ritz_option, int nev, float tol, float* resid,
                  int ncv, float* v, int ldv, int* iparam, int* ipntr,
                  float* workd, float* workl, int lworkl, int& info) {
  detail::snaupd_(&ido, detail::convert_to_char(bmat_option), &n,
                   detail::convert_to_char(ritz_option), &nev, &tol, resid,
                   &ncv, v, &ldv, iparam, ipntr, workd, workl, &lworkl, &info);
}

inline void neupd(bool rvec, howmny const howmny_option, int* select, float* dr,
                  float* di, float* z, int ldz, float sigmar, float sigmai,
                  bmat const bmat_option, int n, which const ritz_option,
                  int nev, float tol, float* resid, int ncv, float* v, int ldv,
                  int* iparam, int* ipntr, float* workd, float* workl,
                  int lworkl, int& info) {
  detail::sneupd_(&rvec, detail::convert_to_char(howmny_option), select, dr,
                     di, z, &ldz, &sigmar, &sigmai,
                   detail::convert_to_char(bmat_option), &n,
                   detail::convert_to_char(ritz_option), &nev, &tol, resid,
                   &ncv, v, &ldv, iparam, ipntr, workd, workl, &lworkl, &info);
}

inline void naupd(int& ido, bmat const bmat_option, int n,
                  which const ritz_option, int nev, double tol, double* resid,
                  int ncv, double* v, int ldv, int* iparam, int* ipntr,
                  double* workd, double* workl, int lworkl, int& info) {
  detail::dnaupd_(&ido, detail::convert_to_char(bmat_option), &n,
                   detail::convert_to_char(ritz_option), &nev, &tol, resid,
                   &ncv, v, &ldv, iparam, ipntr, workd, workl, &lworkl, &info);
}

inline void neupd(bool rvec, howmny const howmny_option, int* select,
                  double* dr, double* di, double* z, int ldz, double sigmar,
                  double sigmai, bmat const bmat_option, int n,
                  which const ritz_option, int nev, double tol, double* resid,
                  int ncv, double* v, int ldv, int* iparam, int* ipntr,
                  double* workd, double* workl, int lworkl, int& info) {
  detail::dneupd_(&rvec, detail::convert_to_char(howmny_option),
                  select, dr, di, z, &ldz, &sigmar, &sigmai,
                  detail::convert_to_char(bmat_option), &n,
                  detail::convert_to_char(ritz_option), &nev, &tol, resid,
                  &ncv, v, &ldv, iparam, ipntr, workd, workl,
                  &lworkl, &info);
}

inline void naupd(int& ido, bmat const bmat_option, int n,
                  which const ritz_option, int nev, float tol,
                  std::complex<float>* resid, int ncv, std::complex<float>* v,
                  int ldv, int* iparam, int* ipntr, std::complex<float>* workd,
                  std::complex<float>* workl, int lworkl,
                  float* rwork, int& info) {
  detail::cnaupd_(&ido, detail::convert_to_char(bmat_option), &n,
                   detail::convert_to_char(ritz_option), &nev, &tol,
                   resid, &ncv, v, &ldv, iparam, ipntr, workd,
                   workl, &lworkl, rwork, &info);
}

inline void neupd(bool rvec, howmny const howmny_option, int* select,
                  std::complex<float>* d, std::complex<float>* z, int ldz,
                  std::complex<float> sigma, std::complex<float>* workev,
                  bmat const bmat_option, int n, which const ritz_option,
                  int nev, float tol, std::complex<float>* resid, int ncv,
                  std::complex<float>* v, int ldv, int* iparam, int* ipntr,
                  std::complex<float>* workd, std::complex<float>* workl,
                  int lworkl, float* rwork, int& info) {
  detail::cneupd_(&rvec, detail::convert_to_char(howmny_option), select,
                   d, z, &ldz, &sigma, workev,
                   detail::convert_to_char(bmat_option), &n,
                   detail::convert_to_char(ritz_option), &nev, &tol,
                   resid, &ncv, v, &ldv, iparam, ipntr,
                   workd, workl, &lworkl, rwork, &info);
}

inline void naupd(int& ido, bmat const bmat_option, int n,
                  which const ritz_option, int nev, double tol,
                  std::complex<double>* resid, int ncv, std::complex<double>* v,
                  int ldv, int* iparam, int* ipntr, std::complex<double>* workd,
                  std::complex<double>* workl, int lworkl,
                  double* rwork, int& info) {  
  detail::znaupd_(&ido, detail::convert_to_char(bmat_option), &n,
                  detail::convert_to_char(ritz_option), &nev, &tol,
                  resid, &ncv, v, &ldv, iparam, ipntr,
                  workd, workl, &lworkl,
                  rwork, &info);
}

inline void neupd(bool rvec, howmny const howmny_option, int* select,
                  std::complex<double>* d, std::complex<double>* z, int ldz,
                  std::complex<double> sigma, std::complex<double>* workev,
                  bmat const bmat_option, int n, which const ritz_option,
                  int nev, double tol, std::complex<double>* resid, int ncv,
                  std::complex<double>* v, int ldv, int* iparam, int* ipntr,
                  std::complex<double>* workd, std::complex<double>* workl,
                  int lworkl, double* rwork, int& info) {
  detail::zneupd_(&rvec, detail::convert_to_char(howmny_option), select,
                  d, z, &ldz, &sigma, workev,
                  detail::convert_to_char(bmat_option), &n,
                  detail::convert_to_char(ritz_option), &nev, &tol,
                  resid, &ncv, v, &ldv, iparam, ipntr,
                  workd, workl, &lworkl, rwork, &info);
}


#endif //  ARPACK_HPP 
