#ifndef ARPACKEIGENOVERLOADS_H
#define ARPACKEIGENOVERLOADS_H

#include "arpack.hpp"
#include <boost/numeric/conversion/cast.hpp>

#include <Eigen/Dense>
#include <Eigen/Sparse>

// Some type definitions I will be using internally
template<typename Scalar>
using DynamicMatrix = Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>;
template<typename Scalar>
using DynamicVector = Eigen::Matrix<Scalar, Eigen::Dynamic, 1>;
// Interface between Eigen and Arpack
template<typename Scalar>
using DynamicDvec = Eigen::Matrix<Scalar, Eigen::Dynamic, 1>;

// Since c++11, it is guaranteed, that reinterpret casting this way works
// --> refernce to member of complex type!
template<typename Scalar>
Scalar & re(std::complex<Scalar> & c) {
  return reinterpret_cast<Scalar (&)[2]>(c)[0];
}

template<typename Scalar>
Scalar & im(std::complex<Scalar> & c) {
  return reinterpret_cast<Scalar (&)[2]>(c)[1];
}

/*
  First overload the unsymmetric routines with the corresponding eigen types
  Use fixed size types where possible. Logically choose vectors vs matrices based
  on arpack docs.
*/

inline int naupd(int ido, bmat bmat_option,
                 which ritz_option, int nev, float tol,
                 DynamicDvec<float> & resid,
                 DynamicMatrix<float> & v,
                 Eigen::Matrix<int, 11, 1> & iparam,
                 Eigen::Matrix<int, 14, 1> & ipntr,
                 DynamicDvec<float> & workd,
                 DynamicDvec<float> & workl,
                 DynamicDvec<float> & /*rwork*/,
                 int & info) {
  naupd(ido, bmat_option,
	boost::numeric_cast<int>(v.rows()),
	ritz_option, nev, tol,
        resid.data(),
	boost::numeric_cast<int>(v.cols()),
	v.data(),
	boost::numeric_cast<int>(v.rows()),
	iparam.data(),
        ipntr.data(), workd.data(), workl.data(),
	boost::numeric_cast<int>(workl.size()), info);
  return ido;
}

inline int naupd(int ido, bmat bmat_option,
                 which ritz_option, int nev, float tol,
                 DynamicDvec<std::complex<float> > & resid,
                 DynamicMatrix<std::complex<float> > & v,
                 Eigen::Matrix<int, 11, 1> & iparam,
                 Eigen::Matrix<int, 14, 1> & ipntr,
                 DynamicDvec<std::complex<float> > & workd,
                 DynamicDvec<std::complex<float> > & workl,
                 DynamicDvec<float> & rwork,
                 int & info) {
  naupd(ido, bmat_option,
	boost::numeric_cast<int>(v.rows()),
	ritz_option, nev, tol,
        resid.data(),
	boost::numeric_cast<int>(v.cols()),
	v.data(),
	boost::numeric_cast<int>(v.rows()),
	iparam.data(),
        ipntr.data(), workd.data(), workl.data(),
	boost::numeric_cast<int>(workl.size()),
        rwork.data(), info);
  return ido;
}

inline int naupd(int ido, bmat bmat_option,
                 which ritz_option, int nev, double tol,
                 DynamicDvec<double> & resid,
                 DynamicMatrix<double> & v,
                 Eigen::Matrix<int, 11, 1> & iparam,
                 Eigen::Matrix<int, 14, 1> & ipntr,
                 DynamicDvec<double> & workd,
                 DynamicDvec<double> & workl,
                 DynamicDvec<double> & /*rwork*/,
                 int & info) {
  naupd(ido, bmat_option,
	boost::numeric_cast<int>(v.rows()),
	ritz_option, nev, tol,
        resid.data(),
	boost::numeric_cast<int>(v.cols()),
	v.data(),
	boost::numeric_cast<int>(v.rows()),
	iparam.data(),
        ipntr.data(), workd.data(), workl.data(),
	boost::numeric_cast<int>(workl.size()),
	info);
  return ido;
}

inline int naupd(int ido, bmat bmat_option,
                 which ritz_option, int nev, double tol,
                 DynamicDvec<std::complex<double> > & resid,
                 DynamicMatrix<std::complex<double> > & v,
                 Eigen::Matrix<int, 11, 1> & iparam,
                 Eigen::Matrix<int, 14, 1> & ipntr,
                 DynamicDvec<std::complex<double> > & workd,
                 DynamicDvec<std::complex<double> > & workl,
                 DynamicDvec<double> & rwork,
                 int & info) {
  naupd(ido, bmat_option, boost::numeric_cast<int>(v.rows()), ritz_option, nev, tol,
        resid.data(), boost::numeric_cast<int>(v.cols()), v.data(),
	boost::numeric_cast<int>(v.rows()), iparam.data(),
        ipntr.data(), workd.data(), workl.data(), boost::numeric_cast<int>(workl.size()),
        rwork.data(), info);
  return ido;
}

inline void neupd(bool rvec, howmny const howmny_option,
                  DynamicDvec<int> select,
                  DynamicDvec<std::complex<float> > & d,
                  DynamicMatrix<float> & z,
                  std::complex<float> sigma,
                  DynamicDvec<float> & /*workev*/,
                  bmat bmat_option,
                  which ritz_option,
                  int nev, float tol,
                  DynamicDvec<float> & resid,
                  DynamicMatrix<float> & v,
                  Eigen::Matrix<int, 11, 1> & iparam,
                  Eigen::Matrix<int, 14, 1> & ipntr,
                  DynamicDvec<float> & workd,
                  DynamicDvec<float> & workl,
                  DynamicDvec<float> & /*rwork*/,
                  int & info) {
  neupd(rvec, howmny_option,
        reinterpret_cast<int *>(select.data()),
        d.real().data(), d.imag().data(),
        z.data(), boost::numeric_cast<int>(z.rows()),
        re(sigma), im(sigma),
        bmat_option, boost::numeric_cast<int>(v.rows()), ritz_option,
        nev, tol, resid.data(), boost::numeric_cast<int>(v.cols()),
        v.data(), boost::numeric_cast<int>(v.rows()), iparam.data(), ipntr.data(),
        workd.data(), workl.data(),
        boost::numeric_cast<int>(workl.size()), info);
}

inline void neupd(bool rvec, howmny const howmny_option,
                  DynamicDvec<int> select,
                  DynamicDvec<std::complex<double> > & d,
                  DynamicMatrix<double> & z,
                  std::complex<double> sigma,
                  DynamicDvec<double> & /*workev*/,
                  bmat bmat_option,
                  which ritz_option,
                  int nev, double tol,
                  DynamicDvec<double> & resid,
                  DynamicMatrix<double> & v,
                  Eigen::Matrix<int, 11, 1> & iparam,
                  Eigen::Matrix<int, 14, 1> & ipntr,
                  DynamicDvec<double> & workd,
                  DynamicDvec<double> & workl,
                  DynamicDvec<double> & /*rwork*/,
                  int & info) {
  neupd(rvec, howmny_option,
        reinterpret_cast<int *>(select.data()),
        d.real().data(), d.imag().data(),
        z.data(), boost::numeric_cast<int>(z.rows()),
        re(sigma), im(sigma),
        bmat_option, boost::numeric_cast<int>(v.rows()), ritz_option,
        nev, tol, resid.data(), boost::numeric_cast<int>(v.cols()),
        v.data(), boost::numeric_cast<int>(v.rows()), iparam.data(), ipntr.data(),
        workd.data(), workl.data(),
        boost::numeric_cast<int>(workl.size()), info);
}

inline void neupd(bool rvec, howmny const howmny_option,
                  DynamicDvec<int> select,
                  DynamicDvec<std::complex<float> > & d,
                  DynamicMatrix<std::complex<float> > & z,
                  std::complex<float> sigma,
                  DynamicDvec<std::complex<float> > & workev,
                  bmat bmat_option,
                  which ritz_option,
                  int nev, float tol,
                  DynamicDvec<std::complex<float> > & resid,
                  DynamicMatrix<std::complex<float> > & v,
                  Eigen::Matrix<int, 11, 1> & iparam,
                  Eigen::Matrix<int, 14, 1> & ipntr,
                  DynamicDvec<std::complex<float> > & workd,
                  DynamicDvec<std::complex<float> > & workl,
                  DynamicDvec<float> & rwork,
                  int & info) {
  neupd(rvec, howmny_option,
        select.data(),
        d.data(), z.data(), boost::numeric_cast<int>(z.rows()),
        sigma, workev.data(),
        bmat_option, boost::numeric_cast<int>(v.rows()), ritz_option,
        nev, tol, resid.data(), boost::numeric_cast<int>(v.cols()),
        v.data(), boost::numeric_cast<int>(v.rows()), iparam.data(), ipntr.data(),
        workd.data(), workl.data(),
        boost::numeric_cast<int>(workl.size()), rwork.data(), info);
}

inline void neupd(bool rvec, howmny const howmny_option,
                  DynamicDvec<int> select,
                  DynamicDvec<std::complex<double> > & d,
                  DynamicMatrix<std::complex<double> > & z,
                  std::complex<double> sigma,
                  DynamicDvec<std::complex<double> > & workev,
                  bmat bmat_option,
                  which ritz_option,
                  int nev, double tol,
                  DynamicDvec<std::complex<double> > & resid,
                  DynamicMatrix<std::complex<double> > & v,
                  Eigen::Matrix<int, 11, 1> & iparam,
                  Eigen::Matrix<int, 14, 1> & ipntr,
                  DynamicDvec<std::complex<double> > & workd,
                  DynamicDvec<std::complex<double> > & workl,
                  DynamicDvec<double> & rwork,
                  int & info) {
  neupd(rvec, howmny_option,
        select.data(),
        d.data(), z.data(), boost::numeric_cast<int>(z.rows()),
        sigma, workev.data(),
        bmat_option, boost::numeric_cast<int>(v.rows()), ritz_option,
        nev, tol, resid.data(), boost::numeric_cast<int>(v.cols()),
        v.data(), boost::numeric_cast<int>(v.rows()), iparam.data(), ipntr.data(),
        workd.data(), workl.data(),
        boost::numeric_cast<int>(workl.size()), rwork.data(), info);
}


#endif /* ARPACKEIGENOVERLOADS_H */

