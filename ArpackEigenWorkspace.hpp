#ifndef ARPACKEIGENWORKSPACE_H
#define ARPACKEIGENWORKSPACE_H

#include "ArpackEigenOverloads.hpp"
#include <Eigen/Dense>
#include <Eigen/Sparse>
// #include "types.hpp"

/*
  Workspace for simple interaction with arpack's nonsymmetric routines
 */
template<typename Scalar>
struct UnsymWorkspace {
  using RealScalar = typename Eigen::NumTraits<Scalar>::Real;
  using ComplexScalar = std::complex<RealScalar>;
  using iparam_t = Eigen::Matrix<int, 11, 1>;
  using ipntr_t = Eigen::Matrix<int, 14, 1>;
  using dense_mat = DynamicMatrix<Scalar>;
  using cdense_vec = DynamicDvec<ComplexScalar>;
  using dense_vec = DynamicDvec<Scalar>;
  using Map_t = Eigen::Map<dense_vec>;

 private:  // WORKSPACE VARIABLES
  // Size of the problem
  int m_n;
  // Number of eigenvectors/eigenvalues
  int m_nev;
  // Space needed for calculation
  int m_ncv;
 
  // Compute ritz vectors or schur vectors at all
  bool m_rvec;
  /*
    'A': Compute NEV Ritz vectors;
    'P': Compute NEV Schur vectors;
    'S': compute some of the Ritz vectors, specified
    by the logical array SELECT
  */
  howmny m_howmny_option;
  // BMAT = 'I' -> standard eigenvalue problem, 'G' -> generalized eigenvalue
  bmat m_bmat_option;
  // Which do you want to calculate --> LM etc.
  which m_ritz_option;
  // Tolerance
  RealScalar m_tol;
  // If IPARAM(7) = 3 then SIGMA represents the shift
  ComplexScalar m_sigma;

  // Select, which ritz vectors to calculate
  DynamicDvec<int> m_select;
  // dimension NEV+1. Ritz approximants
  cdense_vec m_d;
  // N by NEV array. Ritz eigenvectors.
  dense_mat m_z;
  // Dimension 3*NCV.
  dense_vec m_workev;
  // Dimension N
  dense_vec m_resid;
  // N by NCV. Final Arnoldi basis vectors
  dense_mat m_v;

  // See arpack docs for details. Main I/O comm channel.
  iparam_t m_iparam;
  // Ipointer. Used internally.
  ipntr_t m_ipntr;
  // Dimension 3*N
  dense_vec m_workd;
  // Dimension 3*NCV**2 + 5*NCV
  dense_vec m_workl;
  // Length NCV
  DynamicDvec<RealScalar > m_rwork;
  // INFO equal zero
  int m_info;
  // finished ?
  bool completed;

 public:

  ~UnsymWorkspace() {
    /*SHOW(m_z);
    LOG(logINFO) << "m_z\n" << m_z.real().sparseView() << std::endl;
    std::cout << "m_d: " << &m_d << std::endl;
    std::cout << "m_z: " << &m_z << std::endl;
    std::cout << "m_workev: " << &m_workev << std::endl;
    std::cout << "m_resid: " << &m_workev << std::endl;
    std::cout << "m_v: " << &m_v << std::endl;
    std::cout << "m_workd: " << &m_workd << std::endl;
    std::cout << "m_workl: " << &m_workl << std::endl;
    std::cout << "m_rwork: " << &m_rwork << std::endl;*/
  }

  bool calculate_vectors() const {
    return m_rvec;
  }

  UnsymWorkspace(int n, int nev, bool rvec,
                 howmny howmny_option, bmat bmat_option,
                 which ritz_option,
                 RealScalar tol = 0.0,
                 int mode = 1,
                 int maxiter = 0,  // Do this properly
                 ComplexScalar sigma = 0.0,
                 bool provide_shifts = false,
                 const dense_vec & resid = dense_vec(),
                 const DynamicDvec<int> & select = DynamicDvec<int>())
      :m_n(n),
       m_nev(nev),
       m_ncv(std::min(2 * nev + 1, n)),
       m_rvec(rvec),
       m_howmny_option(howmny_option),
       m_bmat_option(bmat_option),
       m_ritz_option(ritz_option),
       m_tol(tol),
       m_sigma(sigma),
       m_d(m_nev + 1),
       m_z((rvec ? n : n), m_nev),
       m_workev(3 * m_ncv),
       m_resid(resid),
       m_v(m_n, m_ncv),
       m_iparam(iparam_t::Zero()),
       m_ipntr(ipntr_t::Zero()),
       m_workd(3 * m_n),
       m_workl(static_cast<int>(3 * std::pow(m_ncv, 2.0) + 5 * m_ncv)),
       m_rwork(m_ncv),
       completed(false) {

    if (select.size() == m_ncv) {
      m_select(select);
    } else {
      m_select = DynamicDvec<int>(m_ncv);
    }

    if (resid.size() == 0) {
      m_resid.resize(m_n);
      m_info = 0;
    } else {
      m_info = 1;
    }

    if (maxiter == 0) maxiter = 100 * nev;

    // DEBUGGING reasons
    m_d.setZero();
    m_z.setZero();
    m_workev.setZero();
    m_v.setZero();
    m_workd.setZero();
    m_workl.setZero();
    m_rwork.setZero();
    m_resid.setZero();

    m_iparam(0) = (provide_shifts ? 0 : 1);
    m_iparam(2) = maxiter;
    m_iparam(3) = 1;
    m_iparam(6) = mode;
  }

  // Calls naupd from the current workspace state
  int call_naupd(int ido) {
    return naupd(ido, m_bmat_option,
                 m_ritz_option, m_nev, m_tol,
                 m_resid, m_v, m_iparam,
                 m_ipntr, m_workd, m_workl, m_rwork,
                 m_info);
  }

  // Calls neupd from the current workspace state
  void call_neupd() {
    neupd(m_rvec, m_howmny_option,
          m_select, m_d, m_z, m_sigma, m_workev,
          m_bmat_option, m_ritz_option,
          m_nev, m_tol, m_resid, m_v,
          m_iparam, m_ipntr, m_workd,
          m_workl, m_rwork, m_info);
    completed = true;
  }

  // number of Arnoldi update iterations taken
  int niter() const {
    return m_iparam(2);
  }

  // number of "converged" Ritz values
  int nconv() const {
    return m_iparam(4);
  }

  // total number of OP*x operations
  int num_op() const {
    return m_iparam(8);
  }

  // total number of B*x operations if BMAT='G'
  int num_bop() const {
    return m_iparam(9);
  }

  // total number of steps of re-orthogonalization
  int num_reo() const {
    return m_iparam(10);
  }

  // After calling neupd, the eigenvalue approximants may be retrieved
  const cdense_vec & ritz_approximants() const {
    assert(completed && "neupd never called");
    return m_d;
  }

  // After calling neupd, the eigenvector approximants may be retrieved
  const dense_mat & ritz_vectors() const {
    assert(completed && "neupd never called");
    assert(m_rvec && "Ritz values not calculated");
    assert(!(m_howmny_option == howmny::schur_vectors)
           && "Schur form has been calculated, not ritz");
    return m_z;
  }

  // The V matrix used in the Arnoldi procedure
  const dense_mat & arnoldi_basis() const {
    return m_v;
  }

  // Translates the info state into a string for logging purposes
  std::string log_info() const {
    switch (m_info) {
      case 0: return "normal exit";
        break;
      case 1: return "Maximum number of iterations taken";
        break;
      case 2: return "Should not happen";
        break;
      case 3: return "No shifts could be applied during a cycle of the"
            "Implicitly restarted Arnoldi iteration. One possibility"
            "is to increase the size of NCV relative to NEV";
        break;
      case -1: return "N must be positive";
        break;
      case -2: return "NEV must be positive";
        break;
      case -3: return "NCV-NEV >= 2 and less than or equal to N";
        break;
      case -4: return "The maximum number of Arnoldi update"
            "iterations must be greater than zero";
        break;
      case -5: return
          "WHICH must be one of 'LM', 'SM', 'LR', 'SR', 'LI', 'SI'";
        break;
      case -6: return "BMAT must be one of 'I' or 'G'";
        break;
      case -7: return "Length of private work array is not sufficient";
        break;
      case -8: return "Error return from LAPACK eigenvalue calculation";
        break;
      case -9: return "Starting vector is zero";
        break;
      case -10: return "IPARAM(7) must be 1,2,3";
        break;
      case -11: return "IPARAM(7) = 1 and BMAT = 'G' are incompatible";
        break;
      case -12: return "IPARAM(1) must be equal to 0 or 1";
        break;
      case -9999: return "probably user input error";
        break;
    }

    return std::to_string(m_info);
  }

  // returns info
  int info() const {
    return m_info;
  }

  // Returns a map to the X vector mentioned in the docs for the naupd routines
  // Used in reverse communication
  Map_t X() {
    // robert_assert(m_ipntr(0) > 0);
    // robert_assert(m_ipntr(0) + m_n - 1 < m_workd.size());
    Scalar * start = m_workd.data() +  m_ipntr(0) - 1;
    Map_t x_map(start, m_n);
    return x_map;
  }

  // Returns a map to the Y vector mentioned in the docs for the naupd routines
  // Used in reverse communication
  Map_t Y() {
    // robert_assert(m_ipntr(1) > 0);
    // robert_assert(m_ipntr(1) + m_n - 1 < m_workd.size());
    Scalar * start = m_workd.data() +  m_ipntr(1) - 1;
    Map_t y_map(start, m_n);
    return y_map;
  }

  ComplexScalar sigma() const {
    return m_sigma;
  }
};

#endif /* ARPACKEIGENWORKSPACE_H */
