#include "ArpackEigenInterface.hpp"
#include <random>
#include <complex>

using spmat = Eigen::SparseMatrix<std::complex<double>, 0, int>;

int main() {
  std::default_random_engine gen;
  std::uniform_real_distribution<double> dist(0.0,1.0);

  int rows=100;
  int cols=100;

  using T = Eigen::Triplet<double>;
  std::vector<T> tripletList;
  for(int i=0;i<rows;++i)
    for(int j=0;j<cols;++j)
      {
	auto v_ij=dist(gen);
	if(v_ij < 0.1)
	  {
	    tripletList.push_back(T(i,j,v_ij));
	  }
      }
  spmat mat(rows,cols);
  mat.setFromTriplets(tripletList.begin(), tripletList.end());

  UnsymArpack<spmat, 1> arp(mat, 10);
}
